import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { UserRegisterComponent } from './user-register/user-register.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { LayoutComponent } from './layout/layout.component';
import { ChatThemeComponent } from './chat-theme/chat-theme.component';
import { environment } from '../environments/environment';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import{ UserprofileComponent} from './userprofile/userprofile.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewchatComponent } from './newchat/newchat.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AvatarModule } from 'ngx-avatar';
import { PushNotificationsModule } from 'ng-push'; 
 
@NgModule({
  declarations: [
    AppComponent,
    UserRegisterComponent,
    UserLoginComponent,
    LayoutComponent,
    ChatThemeComponent,  
    UserprofileComponent, NewchatComponent  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgbModule,
    NgbPaginationModule,
    PushNotificationsModule,
    AvatarModule,
    NgbAlertModule,
    AngularFontAwesomeModule,
    ToastrModule.forRoot({
      timeOut: 15000,
      positionClass: 'toast-top-right',
      preventDuplicates:true,
      }) 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
