//import { Component, OnInit } from '@angular/core';
import { Component, OnInit ,ChangeDetectionStrategy,ChangeDetectorRef} from '@angular/core';
import {ChatserviceService} from '../chatservice.service';
import { Userclass } from '../userclass';
//import { resolvePtr } from 'dns';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import { Observable } from 'rxjs';
//import { resolve } from 'url';
import { resolve } from 'q';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import * as firebase from 'firebase/app';
import { DoCheck, KeyValueDiffers, KeyValueDiffer } from '@angular/core';
import {Upload} from '../upload';
//import { AngularFireObject, AngularFireList } from 'firebase/app';
import { PipeTransform, Pipe } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {
users : any;
userData : any;
userid : any;
name : any;
loggedUserName : any;
loggedUserObject : any;
loggedUserProfile : any;
loggedUserEmail : any;
otherUsers : any= [];
selectedFile : FileList;
currentUpload: Upload;
firebaseRegisterUserskey : any;
loggedUserData : any;
newData : any;
url : any;
tempUrl : boolean = false;
perUrl : boolean = true;

urls = new Array<string>();
  constructor(private chatservice : ChatserviceService,  private route: ActivatedRoute, private navroute: Router,private http : HttpClient,private ref: ChangeDetectorRef,private toastr: ToastrService) { 

  }

  async ngOnInit() {
    await this.checkUserLogged();
    this.userData = <any>await this.getuserData();
  
    this.users = Object.keys(this.userData).map((key) => { return this.userData[key]});
    this.getUser();
    const dbRef = firebase.database().ref().child('Registered');
  dbRef.on('value',
   (snapshot) => {
    
    // console.log(snapshot)
    this.newData = snapshot.val();
    // //this.ref.detectChanges();
    // console.log(this.newData);
    this.getUser();
    this.ref.detectChanges();
    console.log(snapshot.val())
     }
  );  
  }
  async getUser(){
    let i,j;
    this.users.forEach((data) =>{
        //----------- Match loginuser userid and all user userid ----------------//
          if(data.userId == this.userid){
            this.loggedUserObject = data;
            this.loggedUserName = data.name; 
            this.loggedUserProfile = data.url;
            this.ref.detectChanges();
            this.loggedUserEmail = data.email;           
          }else{
             this.otherUsers.push(data)
          }
    }) 
  }
  
  getuserData(){
    return new Promise((resolve,reject) => {
      this.chatservice.getData().subscribe(
        (res: any) => 
        resolve(res)
      );
    })
  }
  checkUserLogged(){
      // Get userId from router(login user userid) 
      this.userid = this.route.snapshot.paramMap.get('userid');
     // this.chatservice.getUserData(this.userid).subscribe(data => console.log(data))
  }
  submit(arg1,arg2){
   console.log("submit")

   this.name = arg1.value;
   console.log(arg1.value,arg2.value)
   
  let logUserFireKey,i,j,k;
  let loggeduserid = this.userid;
 
  console.log("data in change profile");
 
  let keys = Object.keys(this.userData);
  this.firebaseRegisterUserskey = keys;

  for(i=0;i<this.users.length;i++){
    console.log("1111")
      if(this.users[i].userId == loggeduserid){    
          logUserFireKey = keys[i];
          this.loggedUserData = {
              userId : loggeduserid,
              name : this.name,
              email : this.users[i].email,
              password : this.users[i].password
          }
        break;
      }
  }
  this.chatservice.updateUrlRegister(logUserFireKey,this.loggedUserData).subscribe();
  console.log("MY profile in permant")
  console.log(this.loggedUserProfile )
  this.perUrl = false;
  }

 async updateProfile(event){
   this.tempUrl = true;
  this.urls = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.url = e.target.result;
        }
        reader.readAsDataURL(file);
      }
    }
    console.log("MY profile in temp")
    //console.log(this.loggedUserProfile )
    //console.log("MY PROFILE")
    this.selectedFile = event.target.files;
    let file = this.selectedFile.item(0);
    this.currentUpload = new Upload(file);
    await this.chatservice.uploadProfile(this.currentUpload)
   
  }

  
}
