import { Injectable } from '@angular/core';
import { AngularFireDatabase,AngularFireList } from '@angular/fire/database';
import {Userclass} from './userclass';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { auth } from  'firebase/app';
import { AngularFireAuth } from  "@angular/fire/auth";
import { User } from  'firebase';
import { Router } from '@angular/router';  
import {HttpHeaders} from '@angular/common/http';
import { Observable, of,Subject } from 'rxjs';
import * as firebase from 'firebase/app';
import {Upload} from './upload';
import 'firebase/storage';
import { BehaviorSubject } from 'rxjs';

//import {AngularFire, FirebaseListObservable} from 'angularfire2/database'; 
//import { FirebaseListObservable } from 'firebase';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
const options = {responseType: 'text'}
@Injectable({
  providedIn: 'root'
})
export class ChatserviceService {
  userList : AngularFireList<any>;
  private refresh = new Subject<void>();

 //playerList : AngularFireList<any>;
  test : any;  
  user: User;
  url : any;
  profileUrl : any;
  groupProfileUrl : any;
  updatedGroupProfileUrl : any;
  registerUserDetail : any;
  updateProfileRegister : any;
  asignProfileUrl : any;
  get : any = "geted URl"
  private basePath:string = '/uploads';
  private path:string = '/profile';
  private groupPath : string = '/groupProfile';

 // private uploadTask = firebase.storage.UploadTask;
  uploads: Observable<Upload[]>;
  // downloadURL: Observable<string>;
  downloadURL: Observable<any>;
  //https://fir-demo1-16145.firebaseio.com/Registered.json?orderBy=%22name%22&startAt=%22k%22&endAt=%22k\uf8ff%22&print=pretty
  //public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  
  constructor(private fb : AngularFireDatabase,private http:HttpClient,public  afAuth: AngularFireAuth, private route: Router,) { 

  }
  getData(){
    return this.http.get('https://fir-demo1-16145.firebaseio.com/Registered.json');
  }
  getUserData(userid){
    return this.http.get('https://fir-demo1-16145.firebaseio.com/Registered.json/'+userid);
  }
  getLoggedUserData(key){
      return this.http.post('https://www.googleapis.com/identitytoolkit/v3/relyingparty/getAccountInfo?key=AIzaSyCNXFJKypuSfxyz8j8ppU7sVlvaZPQ8Lrs',key);
  }
  createTable(data){
      return this.http.post('https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyCNXFJKypuSfxyz8j8ppU7sVlvaZPQ8Lrs',data);
  }
  signin(data){
    return this.http.post('https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyCNXFJKypuSfxyz8j8ppU7sVlvaZPQ8Lrs',data);
  }
  putUserId(data){
    this.registerUserDetail = {
      userId : data.userId,
      name : data.name,
      email : data.email,
      password : data.password,
      url : this.profileUrl
    }
    return this.http.post('https://fir-demo1-16145.firebaseio.com/Registered.json/', this.registerUserDetail);
  }
  contact(data){
    console.log(data)
    return this.http.post('https://fir-demo1-16145.firebaseio.com/Contact.json/',data);
  }
  acceptReq(data){
    return this.http.post('https://fir-demo1-16145.firebaseio.com/Contact.json/',data);
  }
  checkReceiverId(){
   // console.log("this is in service method")
    return this.http.get('https://fir-demo1-16145.firebaseio.com/Contact.json');
  }
  putMessages(data){
    return this.http.post('https://fir-demo1-16145.firebaseio.com/Message.json/',data);
  }
  getMessages(): Observable<any>{
    return this.http.get('https://fir-demo1-16145.firebaseio.com/Message.json');
  }
  getGroupData(){
    return this.http.get('https://fir-demo1-16145.firebaseio.com/Group.json');
  }
  updateContact(id,data){
  //  console.log("service"+id);
  //  console.log(data);
    return this.http.put('https://fir-demo1-16145.firebaseio.com/Contact/'+id+'.json',data,httpOptions)
  }
  updateContactNew(id,data){
  //  console.log("service"+id);
  //  console.log(data);
    return this.http.put('https://fir-demo1-16145.firebaseio.com/Contact/'+id+'.json',data,httpOptions)
  }
  updateUrlRegister(key,data){
    if(this.profileUrl != ""){
        this.updateProfileRegister = {
            userId :   data.userId,
            email :  data.email,
            name :   data.name,
            password : data.password,
            url : this.profileUrl       
        }
      return this.http.put('https://fir-demo1-16145.firebaseio.com/Registered/'+key+'.json', this.updateProfileRegister,httpOptions);
   }  
  }

  async UploadFile(fd : Upload,data:any){
  //  console.log("upload service")
    let storageRef = firebase.storage().ref();
    let uploadTask = storageRef.child(`${this.basePath}/${fd.file.name}`).put(fd.file);
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) =>  {
        // upload in progress
        fd.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
      },
      (error) => {
        // upload failed
        console.log(error)
      },
      async () => {
        // upload success
        fd.url = <any> await uploadTask.snapshot.ref.getDownloadURL();    
        console.log("using fd");
        console.log(typeof fd.url);
        console.log(fd.url);
        fd.name = fd.file.name;
        console.log(fd.name);
        console.log("ids")
        console.log(data.userId)
        console.log(data.receiverId)
        data = {
          name : fd.name,
          url : fd.url,
          userId : data.userId ,
          receiverId : data.receiverId,
          message : "",
          type : "",
          date : data.date,
          time : data.time
        }
       // this.saveFileData(fd);
        this.saveFileData(data);
      });
  }
 // private saveFileData(upload : Upload) {
  private saveFileData(upload : any) {
    console.log("save file method");
    console.log("my url is ")
    console.log(upload.url);
    // console.log(url);
    console.log("name is ")
    console.log(upload.name);
 /*this is used*/
   // return this.fb.list(`${this.basePath}/`).push(upload);
   return this.http.post('https://fir-demo1-16145.firebaseio.com/Message.json/',upload).subscribe();  
   // return this.http.post('https://fir-demo1-16145.firebaseio.com/uploads.json/',upload).subscribe();  
  }
  uploadProfile(fd : Upload){
    let storageRef = firebase.storage().ref();
    let uploadTask = storageRef.child(`${this.path}/${fd.file.name}`).put(fd.file);
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) =>  {
        // upload in progress
        fd.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
      },
      (error) => {    
        alert("You can select file size till 1MB");
        console.log(error)      
      },
      async () => {
        // upload success
        fd.url = <any> await uploadTask.snapshot.ref.getDownloadURL();    
        fd.name = fd.file.name      
        this.passRegisterProfile(fd.url);
       // this.asignProfileUrl = this.profileUrl;
      }); 
     
  }
passRegisterProfile(profile){
  console.log("User profile");
  console.log(profile);
  this.profileUrl = profile;
  //this.GetResponse();
};
GetResponse(){
  return this.http.get(this.get);
}
uploadGroupProfile(fd : Upload){
  let storageRef = firebase.storage().ref();
  let uploadTask = storageRef.child(`${this.groupPath}/${fd.file.name}`).put(fd.file);
  uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
    (snapshot) =>  {
      // upload in progress
      fd.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
    },
    (error) => {    
      alert("You can select file size till 1MB");
      console.log(error)      
    },
    async () => {
      // upload success
      fd.url = <any> await uploadTask.snapshot.ref.getDownloadURL();    
      fd.name = fd.file.name     
      this.groupProfileUrl = fd.url; 
      //this.passRegisterProfile(fd.url);
    }); 
    //return this.groupProfileUrl;
}
updateGroupProfile(fd : Upload){
  let storageRef = firebase.storage().ref();
  let uploadTask = storageRef.child(`${this.groupPath}/${fd.file.name}`).put(fd.file);
  uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
    (snapshot) =>  {
       fd.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
    },
    (error) => {    
      alert("You can select file size till 1MB");
      console.log(error)      
    },
    async () => {
      fd.url = <any> await uploadTask.snapshot.ref.getDownloadURL();    
      fd.name = fd.file.name     
      this.updatedGroupProfileUrl = fd.url; 
    }); 
}
InsertGroupProfileInTable(key){
  let groupUrl = JSON.stringify(this.updatedGroupProfileUrl)
  return this.http.put('https://fir-demo1-16145.firebaseio.com/Group/'+key+'/url.json/',groupUrl);
}
updateGroupData(key,name){
  let goupName = JSON.stringify(name)
  return this.http.put('https://fir-demo1-16145.firebaseio.com/Group/'+key+'/name.json/',goupName);
}
searchUser(data){
  //return this.http.get('https://fir-demo1-16145.firebaseio.com/Registered.json?orderBy=%22name%22&startAt=%22k%22&endAt=%22k\uf8ff%22');
   return this.http.get('https://fir-demo1-16145.firebaseio.com/Registered.json?orderBy=%22name%22&startAt=%22'+data+'%22&endAt=%22'+data+'\uf8ff%22');
}
createGroupTable(data){
  let object = {
    name : data.name,
    createBy : data.createBy,
    url : this.groupProfileUrl,
    users : data.users,
    userId : data.userId,
    date : data.date
  }
  return this.http.post('https://fir-demo1-16145.firebaseio.com/Group.json/',object);
}
SetFireKey(argkey){
   let key = JSON.stringify(argkey)
  // key = '-Lh_EGOfr4JJLEaedham';
  return this.http.put('https://fir-demo1-16145.firebaseio.com/Group/'+argkey+'/userId.json/',key);
}
addUserinGroup(data){
  let ob = {
    userId : "123"
  }
//return this.http.post('https://fir-demo1-16145.firebaseio.com/player/-LhTu3qi4iDHTbNJ0IZ9/users.json/',data);
//  return this.http.put('https://fir-demo1-16145.firebaseio.com/player/-LhZqwI-kLI2LmwpMHbG/userId.json/',"123");
}
getUserFromGroup(key){
 // console.log(key)
  //return this.http.get('https://fir-demo1-16145.firebaseio.com/Group/'+key+'.json')
    return this.http.get('https://fir-demo1-16145.firebaseio.com/Group/'+key+'/users.json')
}
updateNewUserInGroup(key,users){
//  let ke = '-LhZqwI-kLI2LmwpMHbG'
    return this.http.put('https://fir-demo1-16145.firebaseio.com/Group/'+key+'/users.json',users);
}
findKeyOfGroup(key){
    //console.log(key)
    return this.http.get('https://fir-demo1-16145.firebaseio.com/Group/'+key+'/createBy.json')
}
messageReaded(key) {
  let read = JSON.stringify('read')
  return this.http.put('https://fir-demo1-16145.firebaseio.com/Message/'+key+'/read.json/',read);
}
  insertRegisteredData(username : Userclass, useremail : Userclass,userpassword : Userclass){
      this.userList.push({
        name : username,
        email : useremail,
        password : userpassword
      })
  }
}
