import * as $ from 'jquery';
import { Component, OnInit ,ChangeDetectionStrategy,ChangeDetectorRef} from '@angular/core';
import {ChatserviceService} from '../chatservice.service';
import { Userclass } from '../userclass';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import { Observable } from 'rxjs';
import { resolve, async } from 'q';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import * as firebase from 'firebase/app';
import { DoCheck, KeyValueDiffers, KeyValueDiffer } from '@angular/core';
import {Upload} from '../upload';
import { PipeTransform, Pipe } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
//import '~font-awesome/css/font-awesome.css';
import 'node_modules/font-awesome/css/font-awesome.css';
import { PushNotificationsService} from 'ng-push';

let userData;
let disparray;
let data;

@Component({
  selector: 'app-newchat',
  templateUrl: './newchat.component.html',
  styleUrls: ['./newchat.component.css']
})
export class NewchatComponent implements OnInit {
  name : string;
  datastring : string;
  key : Userclass;
  oneobj : object;
  users : any;
  id : string;
  checkname : string;
  userid : string;
  loggedUser : any;
  loggedUserData : any;
  loggedUserProfile : any;
  loggedUserarray : any;
  otherUsers : any = [];
  otherUserarray : any;
  contactData : any
  messageData : any;
  messageDataArray : any;
  reversemessageDataArray : any = [];
  recvId : any;
  displayMessage : any = [];
  temp : any = [];
  displayImage : any = [];
  displayMessageObser : Observable<any>;
  displayMessageAccept : any = [];
  userNewMessage : any = [];
  checkClickuserID : any;
  getUrlClickUser : any;
  clickedUserProfile : any;
  clickedUserProfileDuplicate : any;
  deleteUerNewMessageLength : any;
  deleteDisplayMessageLength : any;
  approveContactData : any;
  approveContactDataLogeed : any; 
  request : boolean = true;
  showRequestButton : boolean = true;
  breakFlag : number = 0;
  contactUserId : string;
  contactReceiverId : string;
  contactDataArray : any;
  firebasekey : any;
  Newfirebasekey : any;
  firebaseRegisterUserskey : any;
  userId : any;
  receiverId : any;
  Accept : any;
  sendRequest : boolean = true;
  selectedFile : FileList;
  currentUpload: Upload;
  clickUser_UserId : any;
  clickUser_Profile : any;
  count : any = 0;
  upload : any;
  d : any
  time : any;
  timeAP : any;
  date : any;
  month : any;
  year : any;
  fullDate : any;
  hour : any;
  minute : any;
  seconds : any;
  lastMessage : any;
  lastMessageOfClickUser : any;
  lastMessageOfLoggedUser : any;
  lastMessageOfUserId : any;
  lastFinalMessage : any = [];
  lastMessageDate : any = [];
  lastMessageTime : any = [];
  messagesArray : any = [];
  isUserLoggedIn: boolean;
  leggedUserData : any;
  newData : any = {};
  self : any;
  notification : any = [];
  unseenMessage : number = 0;
  updatedFirebaseData : any;
  newNotificationData : any = [];
  unseenFirebaseMessage : number = 0;
  finalUnseenNotification : any = [];
  finalUnseenNotificationDup : any = [];
  downloadImage : boolean = false;
  download : boolean = false;
  imageUrl : any = [];
  ngName : any = "Raj";
  searchUserDataObject : any;
  searchUserDataArray : any;
  showRecord : boolean = true;
  showRecordNewUser : boolean = true;
  contactTableData : any;
  contactTableDataArray : any;
  userIDArray : any = [];
  receiverIDArray : any = [];
  temparray: any = [];
  getTempData : any;
  insertSearchUserData : any;
  insertSearchUserDataTrued : any;
  updatedContactData : any;
  getClickUserData : any;
  userDataObject : any = [];
  clickuserName : any;
  changeProfile : number = 0;
  AcceptRequest : any;
  approveReq : boolean = false;
  showText : boolean = false;
  showBlockNewUser : boolean = true;
  showBlockGroup : boolean = false;
  allFinalMessage : any = [];
  allFinalMessageLength : any;
  storeGroupUsers : any = [];
  addNewUser : any = [];
  groupObject : any;
  url : any;
  groupTableObject : any;
  groupTableArray : any = [];
  updatedGroupData : any;
  perUserMessageData : any = [];
  lastobject : any;
  GroupFirebasekey : any;
  connectedGroupFireKey : any = [];
  receiverGroup : any;
  isActive : any;
  Name : any;
  groupProfile : any = 'https://firebasestorage.googleapis.com/v0/b/fir-demo1-16145.appspot.com/o/groupProfile%2Fuserprofile.png?alt=media&token=9a7bc9c2-0af6-4f2d-88dd-e17a432ead49';
  connectedUsers : any = [];
  firstChar : any;
  groupUsers : any;
  groupUsersArray : any;
  remainingUserInGroup : any = [];
  showButton : boolean = false;
  creatorOfClickedGroup : any;
  showIcon : boolean =  false;
  closeResult: string;
  showTempUrl : boolean = false;
  tempurl : any;
  displaygroup : number = 0;
  FinalObject : any =  [];
  loggedUserMessage : any = [];
  oldMessage : any;
  newMessage : any = [];
  afterSeeNewMessage : any =  [];
  updatedGroupTableArray: any = [];
  eventType : any;
  createProfileEventType : any;
  displayDefaultProfile : boolean = false;
  requestedPerson : any;
  requestedPersonMessage : any;
  constructor(private chatservice : ChatserviceService,  private route: ActivatedRoute, private navroute: Router,private http : HttpClient,private ref: ChangeDetectorRef,private toastr: ToastrService, private modalService: NgbModal, private _pushNotifications: PushNotificationsService ) {  
    this._pushNotifications.requestPermission();
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  notify(msg,senderID,time){ //our function to be called on click
    console.log(msg,senderID,time)
    let senderObject,senderName;
    let options = { //set options
     // body: "The truth is, I'am Iron Man!",
      time : time,
      body : msg,
      icon: "assets/images/ironman.png" //adding an icon
    }
   senderObject = this.otherUsers.find((data) => {
        return data.userId == senderID;
    });
    console.log(senderObject)
    senderName = senderObject.name;
     this._pushNotifications.create(senderName, options).subscribe( //creates a notification
       // res => console.log(res),
        //sssssserr => console.log(err)
    );
  }
 async ngOnInit() {
    $(document).ready(function() {
      $('#action_menu_btn').click(function() {
        $('.action_menu').toggle();
      });
   });
  
   // ------- call messageResponse method for getting message --------- //
  let getResponse  = <any> await this.getMessage()
  let messageResponsearray = await Object.keys(getResponse).map((key) => { return getResponse[key]});
  this.messageDataArray =<any> messageResponsearray;
  this.messagesArray.push(this.messageDataArray);
    //--------- get response of contact table and asign it to contactResponseData ----------- //
  this.contactTableData =  <any>await this.callcontactMethod().then((res) =>resolve(res));
  this.contactTableDataArray = Object.keys( this.contactTableData).map((key) => { return  this.contactTableData[key]});
  this.groupTableObject = <any> await this.getGroupData();
  this.GroupFirebasekey =  Object.keys(this.groupTableObject);
  this.groupTableArray =<any> await Object.keys(this.groupTableObject).map((key) => { return this.groupTableObject[key]});
  await this.checkUserLogged();
  userData = <any>await this.getuserData();
  //console.log("user id is "+userData.this.userid);
  this.users = Object.keys(userData).map((key) => { return userData[key]});
  
  await this.getUser(this.updatedContactData);
  // const dbRef = firebase.database().ref().child('Message');
  // dbRef.on('value',snap  => this.getReceiver(this.checkname));
  // const dbRef3 = firebase.database().ref().child('Contact');
  //   dbRef3.on('value',
  //     (snapshot) => {
  //     console.log(snapshot.val())
  //     this.findLastMessage()
  //   }); 
   

  const dbRef = firebase.database().ref().child('Message');
  dbRef.on('value',
    (snapshot) => {
   // console.log(snapshot)
    this.newData = snapshot.val(); 
   // console.log(this.newData); 
   
   // console.log(this.getTempData,this.checkname)
    this.changeProfile = 0;
    // this is not present 
   // this.getReceiver(this.getTempData,this.checkname);
   // present 
   // this.findNewMessage(this.newData);
    // present
    this.findLastMessageAfterChange(snapshot.val())
    }); 
    
    const dbRef2 = firebase.database().ref().child('Contact');
    dbRef2.on('value',
      (snapshot) => {
     // console.log("my updated data is ")
      this.updatedContactData = snapshot.val()
     // console.log(snapshot.val())
     //present
      this.getUser(this.updatedContactData);
     //not present 
     // this.findLastMessageAfterChange(snapshot.val());
     // this is present in old but not mendetory
      //this.getReceiver(this.getTempData,this.checkname);    
     // not present 
      this.findAcceptRequest(this.updatedContactData)
     // this.transferData()
    });  
    
    const dbRef3 = firebase.database().ref().child('Group');
    dbRef3.on('value',
      (snapshot) => {
      this.updatedGroupData = snapshot.val();
      // let undefinedValue = undefined;
      // this.getUser(undefinedValue);
      this.UpdateGroupTable(this.updatedGroupData)
      this.transferData()
      //this.findGroup(snapshot.val())
    });  
     // --------- call function for display last message ---------- //
//this.findLastMessage();
 //let diftime = "2019-6-10 15:34:5.0" - "2019-6-10 15:34:5.0"
   
   let dt1 = new Date(2014,10,10);
   let dt2 = new Date(2014,10,7);
     console.log(this.diff_hours(dt1, dt2));
  }
  diff_hours(dt2, dt1) 
 {

  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
  diff /= (60 * 60);
  return Math.abs(Math.round(diff));
  
 }
  openModal(){
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
 }
 closeModal(){
   let span = document.getElementsByClassName("close")[0];
   let modal = document.getElementById("myModal");
   modal.style.display = "none";
   let modalnew = document.getElementById("myModalUser");
   modalnew.style.display = "none";
 }
 openModalForUser(){
  var modal = document.getElementById("myModalUser");
  modal.style.display = "block";
 }
 
findLastMessageAfterChange(arg){
  let i;
  this.messageDataArray = Object.keys(arg).map((key) => { return arg[key]});
  this.findLastMessage()
 // -------- comment above emthod ----------//
}
async findLastMessage(){
  
  let i,k,Msglen,Datelen,Timelen, userDatalen, unreadLength;
  //this.messageDataArray = Object.keys(arg).map((key) => { return arg[key]});
 
   Msglen = this.lastFinalMessage.length;
   Datelen = this.lastMessageDate.length;
   userDatalen = this.userDataObject.length;
   Timelen = this.lastMessageTime.length;
    for(k=0;k<Msglen;k++){
       this.lastFinalMessage.pop();
       this.lastMessageDate.pop();
       // ------ commemnt on below -------------//
       this.userDataObject.pop();
       this.lastMessageTime.pop();
    }
    this.loggedUserMessage = [];
//console.log(this.updatedGroupData)
if(this.updatedGroupData != undefined){
 // this.findGroup(this.updatedGroupData);
}
    this.newNotificationData = [];  
    this.otherUsers.forEach((data,i) => {
    this.newMessage = <any>this.messageDataArray.filter((msg) => {
        return ((msg.userId == data.userId && msg.receiverId == this.userid) || (msg.type == 'GP' && msg.receiverId == data.userId));
    })
    console.log(this.newMessage)
    let unread = this.newMessage.filter((msg) => {
        return msg.read == "unread" && msg.userId != this.userid;
    })
    unreadLength = unread.length;
    console.log(unread);
    if(unreadLength > 0) {
          unread.forEach((data,i) => {
             if(data.receiverId.charAt(0) == "-") {
              this.notify(data.message,data.receiverId,data.time);
             }else {
                this.notify(data.message,data.userId,data.time);
              }
          })
    }
    data.notification = unread.length;
   
})

// ---------- initially this.transferData logic is in here --------------- //
this.transferData();
///console.log(this.userDataObject)
// this is present 
 this.getReceiver(this.getTempData,this.checkname);
//this.ref.detectChanges(); 

}
UpdateGroupTable(arg){
    if(arg!=undefined){
      this.updatedGroupTableArray = Object.keys(arg).map((key) => { return arg[key]});     
    }
}
transferData() {
  this.userDataObject = [];
  console.log(this.otherUsers)
  this.otherUsers.forEach((data,i) => {
    this.loggedUserMessage = this.messageDataArray.slice(0).reverse().find((msg) => {
      return ((msg.userId == this.userid && msg.receiverId == data.userId) || (msg.userId == data.userId && msg.receiverId == this.userid) || (msg.type == 'GP' && msg.receiverId == data.userId));
     })
   
     if(this.loggedUserMessage == undefined) {
       console.log(this.loggedUserMessage)
       console.log(data.date)
      this.loggedUserMessage = {
        date : data.date
      };
     }else{
        if(this.loggedUserMessage.message == "") {
            this.loggedUserMessage.message = this.loggedUserMessage.name;
        }
     }
//console.log(this.loggedUserMessage)  
let object = {
   name : data.name,
   url : data.url,
   userId : data.userId,
   receiverId : data.receiverid,
  // notification : this.finalUnseenNotification[i],
   notification : data.notification,
   message : this.loggedUserMessage.message,
   date : this.loggedUserMessage.date,
   time : this.loggedUserMessage.time,
}
  this.userDataObject.push(object);
})
let getIndex,getObject,name,firstChar,index,lastChar,wholeChar,profileImage;
let b = this.userDataObject;
this.userDataObject = this.sortData(b)  
this.userDataObject.forEach((data,i) => {
    // if(data.date == undefined){
    //   getIndex = i;
    //   getObject = data;
    //   this.userDataObject.splice(getIndex,1)
    //   this.userDataObject.unshift(getObject)
    // }
    if(data.message == undefined) {
       console.log(data)
       data.date = "";
    }
})
  this.findDefaultProfile();
  this.ref.detectChanges();
  console.log(this.userDataObject)
}
findDefaultProfile() {
  let getIndex,getObject,name,firstChar,index,lastChar,wholeChar,profileImage;
  this.userDataObject.forEach((data,i) => {
    if(data.url == "" || data.url == undefined) {
      data.url = "";
      name =  data.name;
      firstChar = name.charAt(0);
      index = name.search(" ");
      if(index == -1) {     
        this.displayDefaultProfile = true
           // profileImage = $('#profileImage').text(firstChar);
           profileImage = firstChar;
           data.Durl = profileImage
           //console.log(data.Durl)
      }else{
       this.displayDefaultProfile = true;
         lastChar = name.charAt(index+1)
         profileImage = firstChar+lastChar
         // profileImage = $('#profileImage').text(wholeChar);
         data.Durl = profileImage
         //console.log(data.Durl)
      }
   }

  })
}
sortData(data) {
  return data.sort((a, b) => {
    return <any>new Date(b.date) - <any>new Date(a.date);
  });
}
findLastMessageGroup(){
  let GpData = [];
  let lastMsg,lastDate,lastTime;
  let GroupMessage = [];
  //console.log(this.otherUsers)
    //console.log(arg)
    this.otherUsers.forEach((user,i) => {
      //console.log(user.createBy)
      if(user.createBy != undefined){
         GpData.push(user)
      this.messageDataArray.filter((data) => {
              if(data.userId == user.userId || data.receiverId == user.userId){
                  GroupMessage.push(data)
                  //console.log(data)
             }
        });
      }
   })
   if(GroupMessage.length !=0 ){
      GroupMessage.forEach((msg,i) => {
        if(msg.message != ""){
             //lastMsg = "";
             lastDate = "";
             lastTime = "";
             lastMsg = msg.message;
            // console.log(lastMsg)
             lastDate = msg.date;
             lastTime = msg.time;
        }else{
             //lastMsg = "";
             lastDate = "";
             lastTime = "";
             lastMsg = msg.name;
             //console.log(lastMsg)
             lastDate = msg.date;
             lastTime = msg.time;
        }
      })
   }
  this.lastFinalMessage.push(lastMsg)
  this.lastMessageDate.push(lastDate)
  this.lastMessageTime.push(lastTime)
}
changeprofile(event){
    this.selectedFile = event.target.files;
    let file = this.selectedFile.item(0);
    this.currentUpload = new Upload(file);
    this.chatservice.uploadProfile(this.currentUpload);
}
go(){
    let logUserFireKey,i,j,k;
    let loggeduserid = this.userid;
    let keys = Object.keys(userData);
    this.firebaseRegisterUserskey = keys;
    for(i=0;i<this.users.length;i++){
        if(this.users[i].userId == loggeduserid){   
          logUserFireKey = keys[i];
          this.leggedUserData = {
            userId : loggeduserid,
            name : this.users[i].name,
            email : this.users[i].email,
            password : this.users[i].password
          }
          break;
        }
    }
    this.chatservice.updateUrlRegister(logUserFireKey,this.leggedUserData).subscribe();
}
downloadFile(data){
}
async onkey(event : any){
  this.ngName = (event.target.value).toLowerCase();
  // const rootRef = firebase.database().ref('Registered');
  // const oneRef = rootRef.orderByChild('name').startAt(this.ngName).endAt(this.ngName+'\uf8ff').on('value',(data) => {
  //     console.log(data.val());
  //    })
  this.showRecord = false;
  if(this.ngName != ""){
    this.searchUserDataObject = await this.getSpecificUser(this.ngName);
     this.searchUserDataArray = Object.keys(this.searchUserDataObject).map((key) => { return this.searchUserDataObject[key]});
     this.searchUserDataArray.forEach((data,i) => {
       if(data.userId == this.userid){    
          this.searchUserDataArray.pop();
       }
     })
  }else{
    this.showRecord = true;
  }
 }
 getSpecificUser(data){
     return new Promise((resolve,reject) => {
       this.chatservice.searchUser(data).subscribe((data) => resolve(data))           
      }) 
 }
 getGroupData(){
   return new Promise((resolve,reject) => {
      this.chatservice.getGroupData().subscribe((data) => resolve(data))
   })
 }
 findGroup(arg){
   let i,j,k;
   let users,creatorId;
   this.groupTableArray = Object.keys(arg).map((key) => { return arg[key]});    
  // console.log(this.groupTableArray) 
    this.groupTableArray.forEach((data,i) => {
      creatorId = data.createBy;
      users = data.users
       if(creatorId != this.userid){
          users.forEach((user,j) => {
              if(user.userId == this.userid){
                  this.otherUsers.push(data)
              }
          })
       }
       users.forEach((usr,k) => {
            if(usr.userId == this.userid) {
             this.connectedGroupFireKey.push(this.GroupFirebasekey[i])
            }
       })
    })
    this.findLastMessage();
 }
  async getUser(arg){
    let k,i,j,userDatalen,message;
    this.connectedUsers = [];
    this.notification = [];
   userDatalen = this.userDataObject.length;
// --------------------  GET LOGGED USER DATA FROM REGISTER TABLE --------------//
    for(i=0;i<this.users.length;i++){
        //----------- Match loginuser userid and all user userid ----------------//
          if(this.users[i].userId == this.userid){
            this.loggedUserData = this.users[i];
            this.loggedUser = this.users[i].name; 
            this.loggedUserProfile = this.users[i].url;
          }
    }   
    
    if(arg != undefined){
      this.contactTableDataArray = Object.keys(arg).map((key) => { return arg[key]});   
      //console.log( this.contactTableDataArray) 
    }
 
// ------------------ GET DATA FROM CONTACT TABLE -------------------- //
// --------------- TO GET ONLY CONTACTABLE USERS ---------------- //
    this.contactTableDataArray.forEach((data,i) => {
 // --------- to check if i send any message ? ----------- //     
       if(data.userId == this.userid){        
       // ------------ whose user that i send message to those ----------------- //         
        //  this.receiverIDArray.push(data.receiverId)
          if(data.type == 'I'){
            this.displaygroup = 0;
            this.getContactUser(data.receiverId);  
          }else{
             this.displaygroup = 1;
             this.getContactGroup(data.receiverId)
          }      
       }
 //------------- if any user send me a message ? ------------ //    
         if(data.receiverId == this.userid){     
            // -------------- users who send message to me --------------------//        
            // this.userIDArray.push(data.userId)
            if(data.type == 'I'){
              this.displaygroup = 0;
              this.getContactUser(data.userId)
            }else{
              this.displaygroup = 1;
              this.getContactGroup(data.userId)
            } 
        }

   });

  // this.otherUsers.forEach((data,i) => {
  //   this.oldMessage = <any>this.messageDataArray.filter((msg) => {
  //        return ((msg.userId == data.userId && msg.receiverId == this.userid) || (msg.type == 'GP' && msg.receiverId == data.userId));
  //     })
  //   let object = {
  //      userId :  data.userId,
  //      message : this.oldMessage.length
  //   }
  //   this.notification.push(object)
  // })
   console.log(this.otherUsers)
  this.otherUsers.forEach((data,i) => {
      this.firstChar = data.userId.charAt(0);
      if(this.firstChar != '-') {
        this.connectedUsers.push(data);
      }
  })
  if(this.displaygroup == 1) {
    this.transferData();
  }
}

 async getContactUser(argID){
   //console.log(argID)
   let flagUser = 0;
    let i,j;
      for(i=0;i<this.users.length;i++){
        if(this.users[i].userId == argID){
            if(this.otherUsers.length == 0){
              this.otherUsers.push(this.users[i])
              this.ref.detectChanges();       
              break;
            }else{
              for(j=0;j<this.otherUsers.length;j++){             
                  if(this.otherUsers[j].userId != argID){
                  flagUser = 1;               
                  }else{                  
                    flagUser = 0;                 
                    break;
                  }
              }
              if(flagUser == 1){               
                this.otherUsers.push(this.users[i]);
                this.ref.detectChanges();
              }
            }
        }
      }    
  }
//--------- get contatcs of group ----------//
  getContactGroup(argID){
    //console.log(argID)
    let flagUser = 0;
    let i,j;
    if(this.updatedGroupData != undefined){
      this.groupTableObject = this.updatedGroupData;
      this.groupTableArray = Object.keys(this.updatedGroupData).map((key) => { return this.updatedGroupData[key]}); 
     // console.log(this.groupTableArray)
    }
    let groupkeyarrayDup = Object.keys(this.groupTableObject);
    let groupkey = groupkeyarrayDup[0];
    console.log(this.groupTableArray)
      for(i=0;i<this.groupTableArray.length;i++){
        if(groupkeyarrayDup[i] == argID){
            if(this.otherUsers.length == 0){
              this.otherUsers.push(this.groupTableArray[i])
              break;
            }else{
              for(j=0;j<this.otherUsers.length;j++){      
                  if(this.otherUsers[j].userId != argID){
                  flagUser = 1;               
                  }else{                  
                    flagUser = 0;      
                    break;
                  }
              }
              if(flagUser == 1){
                if(this.groupTableArray[i].userId != ""){
                  this.otherUsers.push(this.groupTableArray[i]);
                }
              }
             
            }
        }
      }    
  }
  createGroup(i,event){
    let checkuser;
    checkuser = event.target.checked;
    if(this.storeGroupUsers.length == 0){
     this.storeGroupUsers.push(this.loggedUserData);
    }
      if( checkuser == true){
        this.storeGroupUsers.push(i)
      }else{
        if( this.storeGroupUsers.length != 0){
          this.storeGroupUsers.forEach((data,j) => {
            if(data == i){
              this.storeGroupUsers.splice(j,1);
            }
          })
        }
      }
  }
  async submitGroup(arg) {
    let contactObject,contactObject2, GroupData;
    this.closeModal()
    this.displaygroup = 1;
    const groupID = [];
    if( this.storeGroupUsers.length > 1 && arg.value != "" ) {    

      this.d = new Date();
      this.date = this.d.getDate();
      this.month = this.d.getMonth()+1;
      this.year = this.d.getFullYear();
      this.hour = this.d.getHours();
      this.minute = this.d.getMinutes();
      this.seconds =  this.d.getSeconds();
      this.time = this.hour+":"+this.minute+":"+this.seconds;   
      this.time =  await this.convertTime(this.time); 
      // this.time =  await this.convertTimePromise(this.time)
      console.log(  this.time)
      this.fullDate = this.year+"-"+this.month+"-"+this.date+" "+this.hour+":"+this.minute+":"+this.seconds+".0";

      this.groupObject = {
          name : arg.value,
          createBy : this.userid,
          url : "",
          userId : "",
          users : this.storeGroupUsers,   
          date :  this.fullDate
      }
      let groupData = <any>await this.group(this.groupObject);
      //console.log(groupData)
      let groupkeyarray = Object.keys(groupData).map((key) => { return groupData[key]});
      this.GroupFirebasekey =  Object.keys(groupData);
      let groupkey = await groupkeyarray[0];
       contactObject = {
        Accept : true,
        type : "G",
        userId : this.userid,
        receiverId : groupkey
      }
      GroupData = <any>await this.chatservice.contact(contactObject).subscribe(data => resolve(data))
      //console.log(GroupData)
      await this.setFireKeyGroupTable(groupkey);
     // console.log(this.storeGroupUsers)
      this.storeGroupUsers.forEach((data,i) => {
         if(data.userId != this.userid){
            contactObject2 = {
               Accept : true,
               type : "G",
               userId :data.userId,
               receiverId : groupkey
            }
            this.chatservice.contact(contactObject2).subscribe(data => resolve(data));
           // console.log(GroupData)
         }
      })
//--------- if any user create group whithout profile then default profile takes first character  ------------- //
    
    }
  
  }
  group(data){
    return new Promise((resolve,reject) => {
      this.chatservice.createGroupTable(data).subscribe(data => resolve(data))
     // this.chatservice.addUserinGroup(this.groupObject).subscribe(data => resolve(data))
    })
  }
  setFireKeyGroupTable(groupkey){ 
    return new Promise((resolve,reject) => {
      this.chatservice.SetFireKey(groupkey).subscribe(data => resolve(data))
    })
  }
  addUserInGroup(i,event){
    let checkNewuser;
    checkNewuser = event.target.checked;
      if( checkNewuser == true){
        this.addNewUser.push(i)
      }else{
        if( this.addNewUser.length != 0){
          this.addNewUser.forEach((data,j) => {
            if(data == i){
              this.addNewUser.splice(j,1);
            }
          })
        }
      }
  }
  async submitaddUserInGroup(){
    let data = []

    if(this.addNewUser.length !=0 ){
        if(this.creatorOfClickedGroup == this.userid){
          this.addNewUser.forEach((data,i) => {
            this.groupUsers.push(data)
          })
       }
    }
  }
  updateNewUserInGroup(key,users){
    return new Promise((resolve,reject) => {
       this.chatservice.updateNewUserInGroup(key,users).subscribe((data) => resolve(data))
    })
  }
  /* my changes data */
  getuserData(){
    return new Promise((resolve,reject) => {
      this.chatservice.getData().subscribe(
        (res: any) => 
        resolve(res)
      );
    })
  }
  removeNotification() {
        let dataarray,dataobject,key,indeDataArray = [],messagereadkey;
        const rootRef = firebase.database().ref('Message');
        const oneRef = rootRef.orderByChild('receiverId').equalTo(this.userid).on('value',(FirebaseData) => {
                          dataobject  = FirebaseData.val();
                          //console.log(dataobject);
                          key = Object.keys(dataobject)
                          dataarray = Object.keys(dataobject).map((key) => { return dataobject[key]});
                          //console.log(dataarray,dataarray.length)
                          dataarray.forEach((dataf,i) => {
                              //console.log(dataf.userId,this.clickUser_UserId)
                              if(dataf.userId == this.clickUser_UserId) {
                                    if(dataf.read == 'unread') {
                                        dataf.key = key[i];
                                        indeDataArray.push(dataf)
                                    }
                              }
                          })
                      })
      console.log(indeDataArray)
      indeDataArray.forEach((data,i) => {
          // console.log(data.key)
          messagereadkey = data.key
          this.chatservice.messageReaded(messagereadkey).subscribe();
      })
  }
  removeNotificationGroup() {
    let dataarray,dataobject,key,indeDataArray = [],messagereadkey;
    const rootRef = firebase.database().ref('Message');
    const oneRef = rootRef.orderByChild('receiverId').equalTo(this.clickUser_UserId).on('value',(FirebaseData) => {
                   dataobject  = FirebaseData.val();
                   key = Object.keys(dataobject)
                   dataarray = Object.keys(dataobject).map((key) => { return dataobject[key]});
                   dataarray.forEach((dataf,i) => {
                    if(dataf.receiverId == this.clickUser_UserId) {
                          if(dataf.read == 'unread') {
                              dataf.key = key[i];
                              indeDataArray.push(dataf)
                          }
                    }
                  })
             })
             //console.log(indeDataArray)
  indeDataArray.forEach((data,i) => {
    // console.log(data.key)
     messagereadkey = data.key
     this.chatservice.messageReaded(messagereadkey).subscribe();
  })
  }
  async getReceiverNew(profile,id){
    let keyObject;
    console.log(profile)
   
    //profile.notification = 0;
    this.allFinalMessage = [];
    let flag = 0,j,i,k;
    this.displaygroup = 0;
    this.firstChar = profile.userId.charAt(0);
    if(this.firstChar == '-'){
        let icons = document.getElementById("plusgp")
        this.showIcon = true;
        icons.style.display = "block";
    }else{
      this.showIcon = false;
    }
    this.showText = true;
    if(this.request == false) {
      this.request = true;
    }
 //---------- this is for not insert record in contact table if record is about of connected user ----------//    
    for(k=0;k<this.userDataObject.length;k++){
      if(this.userDataObject[k].userId == profile.userId){
        this.showBlockNewUser = true;
        break;
      }else{
        this.showBlockNewUser = false;
      }
    }
    this.clickuserName = profile.name;  
    this.clickUser_UserId = profile.userId;
    this.changeProfile = 1;
    this.getClickUserData = profile; 
    this.getUrlClickUser = profile.url;
    if(this.showRecord == false){
      if(profile.userId.charAt(0) == '-'){
          this.insertSearchUserData = {
            userId : this.userid,
            receiverId :  profile.userId,
            Accept : 'false',
            type : 'G'
          }
      }else{
          this.insertSearchUserData = {
            userId : this.userid,
            receiverId :  profile.userId,
            Accept : 'false',
            type : 'I'
          }
      }
        for(j=0;j<this.contactTableDataArray.length;j++){
      // ---------- to check for clicke user is not receiver for any user --------------//   
      // --------- check clicked name userid and receiver id of response from contact table  --------- //
              if(profile.userId != this.contactTableDataArray[j].receiverId){
                // ----- till no record match flag = 1 ------ //
                flag = 1;
              }else {
    // ------ if clicked user is reciver for any user then check that case sender is logged-in user or not------ //                 
                  if(this.userid == this.contactTableDataArray[j].userId) {
                    flag = 0;
                    break;
                  }else{
                    flag = 1;
                  }
              }       
         }
        if(flag == 1) {
          await this.chatservice.contact(this.insertSearchUserData).subscribe(data => resolve(data))          
        }
    }
   // ----------------- remove notification if it is----------------------- // 
  if(profile.notification > 0) {  
      this.removeNotification();
  }
  if(this.firstChar == '-') {
    if(profile.notification > 0) {  
      this.removeNotificationGroup();
     }
  }
 //--------- for getting sifferent king of data 
      //let keyarray = Object.keys(this.newData).map((key) => ({ [key]: this.newData[key] }));
      //----------------
  //let keyarray = Object.keys(this.newData).map((key) => ( { [key]: this.newData[key] }));
  
    this.getReceiver(profile,id);  
     if(this.firstChar == '-') {
         await this.findRemainingUserInGroup(profile);
     }
  }

  async getReceiver(dataArg,id){
// ------------ change the userDataObject instead of otherUsers -------------- //
    let i,j,k,l,flag = 0,p=0;
    this.checkname = id;
   
    //present
    this.ref.detectChanges(); 
    this.getTempData = dataArg;
    if(dataArg != undefined){
      //console.log(dataArg.userId);
      for(i=0;i<this.userDataObject.length;i++){
     // -------------- check other user index and click on name(index) -------------- //
        if(this.userDataObject[i].userId == dataArg.userId){    
         // ----------- when login user click on name then find userid of other user and asign it's userid to recvId vareiable --------------//
        // ----------- clicked userid assign  ----------- //
          this.recvId = this.userDataObject[i].userId;
          if(dataArg.userId.charAt(0) == '-'){
            this.contactData = {
                userId : this.userid,
                receiverId :  this.userDataObject[i].userId,
                Accept : 'false',
                type : 'G'
            }  
          }else{
            this.contactData = {
                userId : this.userid,
                receiverId :  this.userDataObject[i].userId,
                Accept : 'false',
                type : 'I'
            }  
          } 
   //--------- get response of contact table and asign it to contactResponseData ----------- //
           let contactResponseData =  <any>await this.callcontactMethod().then((res) =>resolve(res));
           let keys = Object.keys(contactResponseData);
           this.firebasekey = keys;
  //--------   find key from firebase  ------------- //        
          /* for(k=0;k<keys.length;k++){
             console.log(keys[k]);
             //console.log(this.contactResponseModelData[k].Accept)
           }*/
            let array = Object.keys(contactResponseData).map((key) => { return contactResponseData[key]});
            this.contactDataArray = array;
            // ------- call messageResponse method for getting message --------- //
            let getResponse  = <any> await this.getMessage()
            let messageResponsearray = Object.keys(getResponse).map((key) => { return getResponse[key]});
            this.messageDataArray = messageResponsearray;
 //-------------- delete all message before display message --------//
           // -------- this is only for not saw message every time when login user click other user ---------//
            // this.deleteDisplayMessageLength = this.displayMessage.length;
            // for(k=0;k< this.deleteDisplayMessageLength;k++){
            //     this.displayMessage.pop();
            //  }
            this.displayMessage = [];
            //  this.allFinalMessageLength = this.allFinalMessage.length;
            //  if(this.allFinalMessage.length != 0){
            //     for(k=0;k<this.allFinalMessageLength;k++){
            //       this.allFinalMessage.pop();
            //     }
            //   }
            this.allFinalMessage = [];
          //  //------------- initially usermessage is empty so we make condition ----------- // 
          //    if(this.userNewMessage.length != 0){
          //      // ----------- asign usernewmessage length to deleteusernewMessageLength -------//
          //      // ------ because in for loop we delete record so it effect on for loop ---------//
          //       this.deleteUerNewMessageLength = this.userNewMessage.length;
          //       for(k=0;k< this.deleteUerNewMessageLength;k++){
          //          this.userNewMessage.pop();
          //       }  
          //    }
          this.userNewMessage = [];
             this.changeProfile = 0;       
             for(j=0;j<messageResponsearray.length;j++){
               // -------- check clicked user userid and get message response userid  ---------//
               // ----- only check for clicked user send any messages ---- //
                if(this.userDataObject[i].userId == messageResponsearray[j].userId){
                  //console.log(this.userDataObject[i].userId)
                  // ----- clicked user send message to login user  ------//
                    if(messageResponsearray[j].receiverId == this.userid){        
                     for(k=0;k<array.length;k++){
                       this.contactUserId = messageResponsearray[j].userId;
                       this.contactReceiverId = messageResponsearray[j].receiverId;                  
      // ------- check contact table senderid and message table sender id ------------ //
                        if(array[k].userId == messageResponsearray[j].userId){
      // ------ check conatact table receiver id and message table redceiver id ------------- //                    
                            if(array[k].receiverId == messageResponsearray[j].receiverId){
                                if(array[k].Accept == "false"){
                                  this.request = false;   
                                  this.ref.detectChanges();
                                  this.clickedUserProfileDuplicate = dataArg.url;
                                  this.findRequestedPersonMessage()                              
                                }else{
      //-------- check message display only when logged user is sender and clicked user is receiver or logged user is receiver and clicked user is sender                
                                if((this.userid == messageResponsearray[j].userId && dataArg.userId == messageResponsearray[j].receiverId) || (this.userid == messageResponsearray[j].receiverId && dataArg.userId == messageResponsearray[j].userId)){
                               // if(dataArg.userId == messageResponsearray[j].userId || dataArg.userId == messageResponsearray[j].receiverId){
      //------------- only for if user send image then bydefault message is "" so,not display those type of image.therefor don't push those type of record-------------//
              // ----- in if condition message display and in else condition image display ------- //  
                                      this.changeProfile = 1;
                                      //console.log(messageResponsearray[j].time)
                                      this.clickedUserProfile = dataArg.url;   
                                      this.displayMessage.push(messageResponsearray[j]);          
                                     // this.ref.detectChanges();   
                                      this.allFinalMessage.push(messageResponsearray[j])                    
                                      disparray =  this.displayMessage;                                     
                                      this.displayMessageObser =   this.displayMessage;                                                              
                                      this.displayMessageAccept = this.displayMessage;                                   
                                      break; 
                                 //  }
                                  }         
                                }
                            }
                        }
                       }                 
                    }else{
                      // ------- if logeed user send any message to clicked user -------- //
                      /* -------- only for display send request button if clicked user no send message to logged in user
                                  and logged in user no send message to clicked user ---------    */
                        this.sendRequest = false;
                    }                   
                }
                    //------- login user send any message or not ------//
                    if(this.userid == messageResponsearray[j].userId){   
                      //-------  check login user send message to click user ---------//            
                        if(this.userDataObject[i].userId == messageResponsearray[j].receiverId){                      
                          //-------- check message display only when logged user is sender and clicked user is receiver or logged user is receiver and clicked user is sender                
                         
                        //  if((this.userid == messageResponsearray[j].userId && dataArg.userId == messageResponsearray[j].receiverId) || (this.userid == messageResponsearray[j].receiverId && dataArg.userId == messageResponsearray[j].userId)){                   
                           this.userNewMessage.push(messageResponsearray[j]);      
                          // this.ref.detectChanges();      
                            this.allFinalMessage.push(messageResponsearray[j]);
                        //  }  
                        }
                    }
     //------------- group logic here ------------- //
                    if(this.userDataObject[i].userId == messageResponsearray[j].receiverId){
                        if(messageResponsearray[j].receiverId.charAt(0) == "-" && messageResponsearray[j].userId != this.userid){                        
                                this.receiverGroup = "-";
                                this.showBlockGroup = true;
                                this.allFinalMessage.push(messageResponsearray[j])   
                                this.allFinalMessage.forEach((msg,i) => {
                                      this.Name = this.users.find((data,j) => {
                                        return msg.userId == data.userId;
                                      })
                                      msg.sender = this.Name.name
                                })                   
                      }                                  
                    }               
                
             }   
            // ------- get response of contact table and access using for loop ------- //
              for(j=0;j<array.length;j++){
             // ---------- to check for clicke user is not receiver for any user --------------//   
            // --------- check clicked name userid and receiver id of response from contact table  --------- //
                    if(this.userDataObject[i].userId != array[j].receiverId){
                      // ----- till no record match flag = 1 ------ //
                      flag = 1;
                    }else{
    // ------ if clicked user is reciver for any user then check that case sender is logged-in user or not------ //                 
                        if(this.userid == array[j].userId){
                          flag = 0;
                          break;
                        }else{
                          flag = 1;
                        }
                     }       
             }
              if(flag == 1){
                this.chatservice.contact(this.contactData).subscribe(data => resolve(data))                
              }
        }    
    }
  let b = this.allFinalMessage;
  this.allFinalMessage = this.sortMessage(b)
  this.ref.detectChanges();  
  }
 }
 sortMessage(data) {
  return data.sort((a, b) => {
    return <any>new Date(a.date) - <any>new Date(b.date);
  });
}
findRequestedPersonMessage() { 
   this.requestedPerson = this.userDataObject.find((data) => {
       return data.userId = this.clickUser_UserId;
   })
   console.log(this.requestedPersonMessage)
  this.requestedPersonMessage = this.requestedPerson.message;
}
async findRemainingUserInGroup(arg) {
  let i,j,flag = 0;
  this.remainingUserInGroup = [];
    this.groupUsers = <any> await this.getUserFromGroup(arg.userId)
    for(j=0;j<this.otherUsers.length;j++){
       for(i=0;i<this.groupUsers.length;i++){
            if(this.groupUsers[i].userId == this.otherUsers[j].userId){
              flag = 0;
              break;
            }else{
              flag = 1;
            }
       }
       if(flag == 1){
          if(this.otherUsers[j].userId.charAt(0) != "-"){
              this.remainingUserInGroup.push(this.otherUsers[j])
          }
       }
    }
    this.creatorOfClickedGroup = <any> await this.findKeyOfGroup(arg.userId);
    this.ref.detectChanges(); 
  }
getUserFromGroup(key){
  return new Promise((resolve,reject) =>{
    this.chatservice.getUserFromGroup(key).subscribe((data) => resolve(data))
  })
}
findKeyOfGroup(id){
   return new Promise((resolve,reject) => {
     this.chatservice.findKeyOfGroup(id).subscribe((data) => resolve(data))
   })
}
async acceptRequest(){
   let k,j,l,arg = 0;
   this.request = true;
   this.allFinalMessage = [];
   this.clickedUserProfile = this.clickedUserProfileDuplicate;
  for(k=0;k<this.contactDataArray.length;k++){
      if(this.contactUserId == this.contactDataArray[k].userId){
          if(this.contactReceiverId == this.contactDataArray[k].receiverId){
              this.approveContactData = {
                     userId : this.contactUserId,
                     receiverId: this.contactReceiverId,
                     Accept:"true",
                     type : 'I'
              }
               await this.chatservice.updateContact(this.firebasekey[k],this.approveContactData).subscribe();//.subscribe(data => console.log(data));
             this.findAcceptRequest(arg);
              this.request == true;
              for(j=0;j<this.messageDataArray.length;j++){
                // -------- check clicked user userid and get message response userid  ---------//
                // ----- only check for clicked user send any messages ---- //
                 if(this.recvId == this.messageDataArray[j].userId){
                   // ----- clicked user send message to login user  ------//
                     if(this.messageDataArray[j].receiverId == this.userid){   
                      for(k=0;k<this.contactDataArray.length;k++){
       // ------- check contact table senderid and message table sender id ------------ //
                         if(this.contactDataArray[k].userId == this.messageDataArray[j].userId){
       // ------ check conatact table receiver id and message table redceiver id ------------- //                    
                             if(this.contactDataArray[k].receiverId == this.messageDataArray[j].receiverId){
                             // this.messageDataArray[j];
                                 //  this.displayMessage.push(this.messageDataArray[j]);
                                   this.ref.detectChanges();   
                                   this.displayMessageAccept = this.displayMessage;   
                                   this.allFinalMessage.push(this.messageDataArray[j])                                  
                                   break;                           
                             }
                         }
                        }                 
                     }else{
                       // ------- if logeed user send any message to clicked user -------- //
                       /* -------- only for display send request button if clicked user no send message to logged in user
                                   and logged in user no send message to clicked user ---------    */
                        this.sendRequest = false;
                     }                   
                 }
                 //------- login user send any message or not ------//
                 if(this.userid == this.messageDataArray[j].userId){   
                   //-------  check login user send message to click user ---------//            
                     if(this.recvId ==  this.messageDataArray[j].receiverId){                      
                         this.userNewMessage.push( this.messageDataArray[j].message);  
                         this.allFinalMessage.push(this.messageDataArray[j])          
                         this.ref.detectChanges();                                         
                     }
                 }
              }
              break;
          }
      }
   }
   this.request = true;
   this.ref.detectChanges();
   console.log(this.allFinalMessage)
  }
 async findAcceptRequest(arg){
   if(arg != 0){
     this.contactTableDataArray = Object.keys(arg).map((key) => { return  arg[key]});
   }
  let Newkeys = Object.keys(arg);
  this.Newfirebasekey = Newkeys;
    let j,fbkey;
    this.approveReq = false;
    this.insertSearchUserDataTrued = {
      userId : this.userid,
      receiverId : this.clickUser_UserId,
      Accept : 'true',
      type : 'I'
    }
    for(j=0;j<this.contactTableDataArray.length;j++){
      // check logged-in user is receiver of any user ?
      if(this.userid == this.contactTableDataArray[j].receiverId){
        // if loggedin user is receiver of any user then check sender is clicked user ?
        if(this.contactTableDataArray[j].userId ==  this.clickUser_UserId){
        // if clicked user send me a request then pass true in request.
         // this.approveReq = true;
          if(this.contactTableDataArray[j].Accept == "true"){
            this.approveReq = true;       
          }
        }
      }
    }
 if(this.approveReq == true){
    for(j=0;j<this.contactTableDataArray.length;j++){
      if(this.userid == this.contactTableDataArray[j].userId){
        if(this.contactTableDataArray[j].receiverId ==  this.clickUser_UserId){
          if(this.Newfirebasekey[j] != undefined){
         this.chatservice.updateContactNew(this.Newfirebasekey[j], this.insertSearchUserDataTrued).subscribe();
         break;
          }
        }
      }
    }  
  }
 
} 
  refuseRequest(){
    this.request = true;
    this.ref.detectChanges();
    return false;
  }

  handleFileInput(event) {
  // ---------- this is handlefileInput method --------- // 
    this.selectedFile = event.target.files;
 /* -------- this is go method -------*/
    let file = this.selectedFile.item(0);
    this.currentUpload = new Upload(file);
    this.upload = {
      userId :  this.userid,
      receiverId :this.recvId,
      message : "",
      type : "",
      date : this.fullDate,
      time : this.time  
    }
    this.chatservice.UploadFile(this.currentUpload,this.upload); 
   }
   async createProfile(event){
     this.createProfileEventType = event.type;
     
     let files = event.target.files;
     if (files) {
       for (let file of files) {
         let reader = new FileReader();
         reader.onload = (e: any) => {
           this.url = e.target.result;
         }
         reader.readAsDataURL(file);
       }
     }
     console.log("MY profile in temp")
     this.selectedFile = event.target.files;
    
     let file = this.selectedFile.item(0);
     this.currentUpload = new Upload(file);
     await this.chatservice.uploadGroupProfile(this.currentUpload) 
   }
  async updateGroupProfile(event){
    this.eventType = event.type;
    console.log(event,this.eventType)
    this.showTempUrl = true;
    let files = event.target.files;
    console.log(files)
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.tempurl = e.target.result;
        }
        reader.readAsDataURL(file);
      }
    }
    this.selectedFile = event.target.files;
    let file = this.selectedFile.item(0);
    this.currentUpload = new Upload(file);
    this.chatservice.updateGroupProfile(this.currentUpload)
  }
  async updateGroupData(arg){
    let groupNewName = arg.value;
    let getIndex,getObject,name,firstChar,index,lastChar,wholeChar,profileImage;
      //----------- this is for only when user change profile then called ---------------//
      if( this.eventType == "change"){
           await this.submitUpdatedGroupProfile()
           this.userDataObject.forEach((data,i) => {
              if(data.userId == this.clickUser_UserId) {
                  data.url = this.tempurl;
              }
          })
      }
      //-------- get object of clicked group ---------- //
       let getOldGroupObject = this.userDataObject.find((data) => {
            return data.userId == this.clickUser_UserId;
        })
    //-------- if user change the group name then only method called ------------ //    
      if(getOldGroupObject.name != groupNewName) {
            let updatedGroupName =  <any>await this.updateGroupName(this.clickUser_UserId,groupNewName)
            this.userDataObject.forEach((data,i) => {
                if(data.userId == this.clickUser_UserId) {
                    data.name = updatedGroupName;
                    this.clickuserName = updatedGroupName;
                }
              //   if(data.url == "" || data.url == undefined) {
              //     data.url = "";
              //     name =  data.name;
              //     firstChar = name.charAt(0);
              //     index = name.search(" ");
              //     if(index == -1) {     
              //       this.displayDefaultProfile = true
              //          profileImage = firstChar;
              //          data.Durl = profileImage;
              //     }else{
              //        this.displayDefaultProfile = true;
              //        lastChar = name.charAt(index+1)
              //        profileImage = firstChar+lastChar
              //        data.Durl = profileImage
              //     }
              //  }
            })
            this.findDefaultProfile();
      }
  }
  submitUpdatedGroupProfile() {
    return new Promise((resolve,reject) => {
       this.chatservice.InsertGroupProfileInTable(this.clickUser_UserId).subscribe(data => resolve(data));
    })
  }
  updateGroupName(id,groupNewName) {
      return new Promise((resolve,reject) => {
        this.chatservice.updateGroupData(id,groupNewName).subscribe(data => resolve(data))
      })
  }

  callcontactMethod(){
    return new Promise((resolve,reject) => {
      this.chatservice.checkReceiverId().subscribe((data) =>resolve(data));
    })
  }
  getMessage(){
    return new Promise((resolve,reject) => {
      this.chatservice.getMessages().subscribe(data => resolve(data));
    })
  }
  logoutUser(){
    localStorage.removeItem('idToken');
    this.navroute.navigate(['/Login']);
  }

  checkUserLogged(){
    if(localStorage.idToken == null){
       this.navroute.navigate(['/Login']);
    }
    else{
      this.userid = this.route.snapshot.paramMap.get('userid');
    }
  }
  convertTime(time){
    return new Promise((resolve,reject) => {
      if(time){
        console.log("time method")
        time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
          time = time.slice (1);  // Remove full string match value
          time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
          time[0] = +time[0] % 12 || 12; // Adjust hours
        }
    resolve(time.join (''));     
      }
   })
  }
  convertTimePromise(arg){
      return new Promise((resolve,reject) => {
        if(arg) {
         resolve( this.convertTime(arg))
        }
      })
  }
  async submitMessage(msg){
    this.showBlockNewUser = true;
    if(this.recvId == undefined){
      this.recvId = this.getClickUserData.userId;
    }
  
    if(msg.value != ""){
      this.d = new Date();
      this.date = this.d.getDate();
      this.month = this.d.getMonth()+1;
      this.year = this.d.getFullYear();
      this.hour = this.d.getHours();
      this.minute = this.d.getMinutes();
      this.seconds =  this.d.getSeconds();
      this.time = this.hour+":"+this.minute+":"+this.seconds;   
      this.time =  await this.convertTime(this.time); 
      // this.time =  await this.convertTimePromise(this.time)
      console.log(  this.time)
      this.fullDate = this.year+"-"+this.month+"-"+this.date+" "+this.hour+":"+this.minute+":"+this.seconds+".0";
     console.log(this.recvId.charAt(0))
    if(this.recvId.charAt(0) == "-"){
        this.messageData = {
          userId : this.userid,
          receiverId : this.recvId,
          message : msg.value,
          type : 'GP',
          name : "",
          url : "",
          date : this.fullDate,
          time : this.time,
          profile : this.loggedUserProfile,
          read : "unread"
        }  
    }else{
      this.messageData = {
          userId : this.userid,
          receiverId : this.recvId,
          message : msg.value,
          type : 'IND',
          name : "",
          url : "",
          date : this.fullDate,
          time : this.time,
          profile : "",
          read : "unread"       
      }
    }
      this.userNewMessage.push(msg.value);
      await this.chatservice.putMessages(this.messageData).subscribe();
      (<HTMLInputElement>document.getElementById("txt")).value = "";
    }
  }


}
