import { Component, OnInit } from '@angular/core';
import {ChatserviceService } from '../chatservice.service';
import { reject } from 'q';
import { Userclass } from '../userclass';
import {Upload} from '../upload';
import {resolve} from 'q';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {
name : string;
email : string;
password : string;
users : Userclass[];
object : any;
detail : any;
selectedFile : FileList;
currentUpload: Upload;
profileResponse : any;
tempurl : any;
showTempUrl :boolean = false;
defaultProfile : any = 'https://firebasestorage.googleapis.com/v0/b/fir-demo1-16145.appspot.com/o/groupProfile%2Fuserprofile.png?alt=media&token=9a7bc9c2-0af6-4f2d-88dd-e17a432ead49';

  constructor(private chatservice : ChatserviceService ) { }

  async ngOnInit() {
   //   let RegisterdData = await this.getFireBaseData();
   //   console.log("my geted Data is "+RegisterdData);
      console.log("component file")
   /*var x = this.chatservice.createTable();
   x.snapshotChanges().subscribe(item => {
       this.users = [];
       item.forEach(data => {
       var y = data.payload.toJSON()
       y["$key"] = data.key;
       this.users.push(y as Userclass)
     })
   })
  console.log("data is "+this.users);
*/
  }

  async addTable(data1,data2,data3){
      this.object = {
      email : data2,
      password : data3,
      "returnSecureToken":true
    }
   
    let registerData = <any> await this.getUserId(this.object);
    console.log("localid is "+registerData.localId);
    this.detail = {
      userId : registerData.localId,
      name : data1,
      email : data2,
      password : data3    
    }
    this.chatservice.putUserId(this.detail).subscribe(data => console.log(data));
  } 
  getUserId(obj){
    return new Promise((resolve,reject) => {
      this.chatservice.createTable(obj).subscribe(data => resolve(data))
    })
  }

SubmitData(arg1,arg2,arg3){
  
  this.name = arg1.value;
  this.email = arg2.value;
  this.password = arg3.value;
 // console.log(this.name+" "+this.email+" "+this.password);
  if( this.name == "" ||  this.email == "" ||  this.password == ""){
    alert("Please fill the field");
  }else{
      this.addTable(arg1.value,arg2.value,arg3.value);
      //this.chatservice.insertRegisteredData(arg1.value,arg2.value,arg3.value);
      console.log("after submitted data ")
  }
}

handleFileInput(event) {

  let files = event.target.files;
  if (files) {
    for (let file of files) {
      let reader = new FileReader();
      reader.onload = (e: any) => {
        this.tempurl = e.target.result;
      }
      reader.readAsDataURL(file);
    }
  }
  this.showTempUrl = true;
  console.log(this.profileResponse)
  console.log("file upload ");
  this.selectedFile = event.target.files;
  let file = this.selectedFile.item(0);
  this.currentUpload = new Upload(file);
  this.chatservice.uploadProfile(this.currentUpload);
  // this.profileResponse = this.chatservice.GetResponse().subscribe(data => resolve(data))
  // console.log(this.profileResponse)
 // const file : File = event.target.files[0];
  //const metaData = {'',}
 }


}
