import { Component, OnInit ,ChangeDetectionStrategy,ChangeDetectorRef} from '@angular/core';
import {ChatserviceService} from '../chatservice.service';
import { Userclass } from '../userclass';
//import { resolvePtr } from 'dns';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import { Observable } from 'rxjs';
//import { Subject } from 'rxjs/Subject';
//import { resolve } from 'url';
import { resolve, async } from 'q';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import * as firebase from 'firebase/app';
import { DoCheck, KeyValueDiffers, KeyValueDiffer } from '@angular/core';
import {Upload} from '../upload';
//import { AngularFireObject, AngularFireList } from 'firebase/app';
import { PipeTransform, Pipe } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
//import * as firebase from 'firebase/app';

let userData;
let disparray;
let data;
@Component({
  selector: 'app-chat-theme',
  templateUrl: './chat-theme.component.html',
  styleUrls: ['./chat-theme.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
@Pipe({
  name: 'orderBy'
})
export class ChatThemeComponent implements OnInit {
//name : Userclass;
name : string;
datastring : string;
key : Userclass;
oneobj : object;
users : any;
id : string;
checkname : string;
userid : string;
loggedUser : any;
loggedUserProfile : any;
loggedUserarray : any;
otherUsers : any = [];
otherUserarray : any;
contactData : any
messageData : any;
messageDataArray : any;
recvId : any;
displayMessage : any = [];
temp : any = [];
displayImage : any = [];
displayMessageObser : Observable<any>;
displayMessageAccept : any = [];
userNewMessage : any = [];
checkClickuserID : any;
getUrlClickUser : any;
clickedUserProfile : any;
deleteUerNewMessageLength : any;
deleteDisplayMessageLength : any;
approveContactData : any;
approveContactDataLogeed : any; 
request : boolean = true;
showRequestButton : boolean = true;
breakFlag : number = 0;
contactUserId : string;
contactReceiverId : string;
contactDataArray : any;
firebasekey : any;
Newfirebasekey : any;
firebaseRegisterUserskey : any;
userId : any;
receiverId : any;
Accept : any;
sendRequest : boolean = true;
selectedFile : FileList;
currentUpload: Upload;
clickUser_UserId : any;
clickUser_Profile : any;
count : any = 0;
upload : any;
d : any
time : any;
timeAP : any;
date : any;
month : any;
year : any;
fullDate : any;
hour : any;
minute : any;
seconds : any;
lastMessage : any;
lastMessageOfClickUser : any;
lastMessageOfLoggedUser : any;
lastMessageOfUserId : any;
lastFinalMessage : any = [];
lastMessageDate : any = [];
lastMessageTime : any = [];
messagesArray : any = [];
isUserLoggedIn: boolean;
leggedUserData : any;
newData : any = {};
self : any;
notification : any = [];
unseenMessage : number = 0;
updatedFirebaseData : any;
newNotificationData : any = [];
unseenFirebaseMessage : number = 0;
finalUnseenNotification : any = [];
finalUnseenNotificationDup : any = [];
downloadImage : boolean = false;
download : boolean = false;
imageUrl : any = [];
ngName : any = "Raj";
searchUserDataObject : any;
searchUserDataArray : any;
showRecord : boolean = true;
showRecordNewUser : boolean = true;
contactTableData : any;
contactTableDataArray : any;
userIDArray : any = [];
receiverIDArray : any = [];
temparray: any = [];
getTempData : any;
insertSearchUserData : any;
insertSearchUserDataTrued : any;
updatedContactData : any;
getClickUserData : any;
userDataObject : any = [];
clickuserName : any;
changeProfile : number = 0;
AcceptRequest : any;
approveReq : boolean = false;
constructor(private chatservice : ChatserviceService,  private route: ActivatedRoute, private navroute: Router,private http : HttpClient,private ref: ChangeDetectorRef,private toastr: ToastrService) {  

}
  async ngOnInit() {
 // ------- call messageResponse method for getting message --------- //
  let getResponse  = <any> await this.getMessage()
  let messageResponsearray = await Object.keys(getResponse).map((key) => { return getResponse[key]});
  this.messageDataArray =<any> messageResponsearray;
  this.messagesArray.push(this.messageDataArray);
    //--------- get response of contact table and asign it to contactResponseData ----------- //
  this.contactTableData =  <any>await this.callcontactMethod().then((res) =>resolve(res));
  this.contactTableDataArray = Object.keys( this.contactTableData).map((key) => { return  this.contactTableData[key]});

  
  await this.checkUserLogged();
  userData = <any>await this.getuserData();
  //console.log("user id is "+userData.this.userid);
  this.users = Object.keys(userData).map((key) => { return userData[key]});
  
  await this.getUser(this.updatedContactData);
  // const dbRef = firebase.database().ref().child('Message');
  // dbRef.on('value',snap  => this.getReceiver(this.checkname));
  // const dbRef3 = firebase.database().ref().child('Contact');
  //   dbRef3.on('value',
  //     (snapshot) => {
  //     console.log(snapshot.val())
  //     this.findLastMessage()
  //   }); 
   

  const dbRef = firebase.database().ref().child('Message');
  dbRef.on('value',
    (snapshot) => {
   // console.log(snapshot)
    this.newData = snapshot.val(); 
   // console.log(this.newData); 
   
   // console.log(this.getTempData,this.checkname)
    this.changeProfile = 0;
    // this is not present 
   // this.getReceiver(this.getTempData,this.checkname);
    this.findNewMessage(this.newData);
    // present
    this.findLastMessageAfterChange(snapshot.val())
    }); 
    
    const dbRef2 = firebase.database().ref().child('Contact');
    dbRef2.on('value',
      (snapshot) => {
     // console.log("my updated data is ")
      this.updatedContactData = snapshot.val()
     // console.log(snapshot.val())
     //present
      this.getUser(this.updatedContactData);
     //not present 
     // this.findLastMessageAfterChange(snapshot.val());
     // this is present in old but not mendetory
      //this.getReceiver(this.getTempData,this.checkname);    
     // not present 
      this.findAcceptRequest(this.updatedContactData)
    });  
    
     // --------- call function for display last message ---------- //
//this.findLastMessage();
 
  this.otherUsers.forEach((users,i) => {
    this.unseenMessage = 0;
      this.messageDataArray.forEach((messages,j) => {
         if(users.userId == messages.userId){
            if(messages.receiverId == this.userid){
              this.unseenMessage++;
              this.notification[i] = this.unseenMessage;
            }
         }
      })
  })
  //.startAt(this.ngName).endAt(this.ngName+'\uf8ff')
  // this.findAcceptRequest()
}
findLastMessageAfterChange(arg){
  let i;
  this.messageDataArray = Object.keys(arg).map((key) => { return arg[key]});
  this.findLastMessage()
 // -------- comment above emthod ----------//
}
async findLastMessage(){
 
  let i,k,Msglen,Datelen,Timelen,userDatalen;
  //this.messageDataArray = Object.keys(arg).map((key) => { return arg[key]});
 
   Msglen = this.lastFinalMessage.length;
   Datelen = this.lastMessageDate.length;
   userDatalen = this.userDataObject.length;
   Timelen = this.lastMessageTime.length;
    for(k=0;k<Msglen;k++){
       this.lastFinalMessage.pop();
       this.lastMessageDate.pop();
       // ------ commemnt on below -------------//
       this.userDataObject.pop();
       this.lastMessageTime.pop();
    }
  this.otherUsers.map((item,index) =>{   
    for(i=(this.messageDataArray.length-1);i>=0;i--){    
     // ---------- check clicked user send any message ---------- //
            if( item.userId == this.messageDataArray[i].userId){                          
             // console.log(this.messageDataArray[i].receiverId)
                // --------- check clicked user send message to logged in user ------------- //   
                if(this.messageDataArray[i].receiverId == this.userid){                  
                  //---------------  if new user send any message and logged-in user approve his request  then display last messge ----- //   
                  //console.log("check request is ")
                  //console.log(this.request)
                  // if(this.request == true){    
                        //--------- asign last message to variable ------------- //      
                        this.lastMessage =  this.messageDataArray[i].message;
                        // ------- check is last message is image then display image name --------- //      
                        if(this.lastMessage == ""){                     
                          this.lastMessageOfClickUser = this.messageDataArray[i].name;                                  
                          break;
                        }else{
                          this.lastMessageOfClickUser = this.lastMessage;                                
                          break;
                        }   
                  //}
                }
            }
      }
      for(i=this.messageDataArray.length-1;i>=0;i--){   
            // ---------- check logged in  user send any message ---------- //
            if(this.userid == this.messageDataArray[i].userId){       
                // --------- check logged in user send message to clicked user ------------- //   
                if( item.userId == this.messageDataArray[i].receiverId ){               
                     //---------------  if new user send any message and logged-in user approve his request  then display last messge ----- //   
                 //  if(this.request == true){   
                        //--------- asign last message to variable ------------- //      
                        this.lastMessage =  this.messageDataArray[i].message;
                        // ------- check is last message is image then display image name --------- //      
                        if(this.lastMessage == ""){
                          this.lastMessageOfLoggedUser = this.messageDataArray[i].name;                    
                          break;
                        }else{
                          this.lastMessageOfLoggedUser =  this.lastMessage;                                 
                          break;
                        }
                }
            }
          }
          for(i=this.messageDataArray.length-1;i>=0;i--){
       
           if(this.messageDataArray[i].message != ""){ 
          
                if(this.messageDataArray[i].message ==  this.lastMessageOfClickUser){
                   
                    // ------------- push last message of click user ---------- //
                    this.lastFinalMessage.push(this.lastMessageOfClickUser)      
                   //this.ref.detectChanges();
        // ------- this is only beacause for remove old value of last message ---------- //         
                   this.lastMessageOfClickUser = "";
                   this.lastMessageDate.push(this.messageDataArray[i].date);
                  // this.ref.detectChanges();
                   this.messageDataArray[i].date = ""
                   this.lastMessageTime.push(this.messageDataArray[i].time);
                  // this.ref.detectChanges();
                   this.messageDataArray[i].time = "";          
                   break;
                  }
                  if(this.messageDataArray[i].message ==  this.lastMessageOfLoggedUser){
                    // ------------- push last message of logged in user ---------- //
                     this.lastFinalMessage.push(this.lastMessageOfLoggedUser)      
                    // this.ref.detectChanges();
                     // ------- this is only beacause for remove old value of last message ---------- //        
                    this.lastMessageOfLoggedUser = ""
                    this.lastMessageDate.push(this.messageDataArray[i].date);   
                   // this.ref.detectChanges();
                    this.messageDataArray[i].date = ""   
                    this.lastMessageTime.push(this.messageDataArray[i].time);
                    //this.ref.detectChanges();
                    this.messageDataArray[i].time = "";           
                    break;
                  }
            }else{
               if(this.messageDataArray[i].name ==  this.lastMessageOfClickUser){
                // ------------- push last message of click user ---------- //
                this.lastFinalMessage.push(this.lastMessageOfClickUser)   
               // this.ref.detectChanges();   
                // ------- this is only beacause for remove old value of last message ---------- //                
                this.lastMessageOfClickUser = ""
                this.lastMessageDate.push(this.messageDataArray[i].date);
               // this.ref.detectChanges();
                this.messageDataArray[i].date = ""
                this.lastMessageTime.push(this.messageDataArray[i].time);
                //this.ref.detectChanges();
                this.messageDataArray[i].time = "";              
                break;
              }
              if(this.messageDataArray[i].name ==  this.lastMessageOfLoggedUser){
                //------------- push last message of logged in user ---------- //
                this.lastFinalMessage.push(this.lastMessageOfLoggedUser)     
               // this.ref.detectChanges(); 
                //------- this is only beacause for remove old value of last message ---------- //                  
                this.lastMessageOfLoggedUser = ""
                this.lastMessageDate.push(this.messageDataArray[i].date);  
                //this.ref.detectChanges();    
                this.messageDataArray[i].date = ""      
                this.lastMessageTime.push(this.messageDataArray[i].time);
                //this.ref.detectChanges();
                this.messageDataArray[i].time = "";        
                break;
              }
           }
          }    
})
//await this.getTimeAP();
this.otherUsers.forEach((data,i) => {
    let object = {
      name : this.otherUsers[i].name,
      url : this.otherUsers[i].url,
      userId : this.otherUsers[i].userId,
      receiverId : this.otherUsers[i].receiverId,
      message :   this.lastFinalMessage[i],
      date :  this.lastMessageDate[i],
 //------------ if you want to use ordering then use only time ---------- //
      time :  this.lastMessageTime[i],
      notification : this.finalUnseenNotification[i]
//----------- display message in timeAp --------------// 
    //  timeAP : this.getTimeAP(this.lastMessageTime[i])
    }
  this.userDataObject.push(object);
})
console.log(this.userDataObject.notification)
let b = this.userDataObject;

this.userDataObject = this.sortData(b)  
//this.ref.detectChanges(); 
// this is present 
 this.getReceiver(this.getTempData,this.checkname);
 //this.ref.detectChanges(); 

// this.d = new Date();
// this.hour = this.d.getHours();
// this.minute = this.d.getMinutes();
// this.seconds =  this.d.getSeconds();
// this.time = this.hour+":"+this.minute+":"+this.seconds;   

}
sortData(data) {
  return data.sort((a, b) => {
    return <any>new Date(b.date) - <any>new Date(a.date);
  });
}
getTimeAP(){
  this.otherUsers.forEach((data,i) => {
    this.lastMessageTime[i] = this.convertTime(this.lastMessageTime[i]);
  });
  // let i,d,t,h=0,m=0,f=0;
  // t = arg;
  // for(i=0;i<arg.length;i++){
  //     d = arg.charAt(i);
  //     if(d == ":"){
  //         f=1;
  //         continue;
  //     }else if(f==0){     
  //         d = parseInt(d);
  //         h = (h*10)+d;         
  //     }else{
  //         d  = parseInt(d);
  //         m = (m*10)+d;
  //     }
  // } 
  // this.hour = h;
  // this.minute = m;
}
findNewMessage(updateddata){
 //  console.log("New Method")
   this.updatedFirebaseData = Object.keys(updateddata).map((key) => { return updateddata[key]});
   
   this.otherUsers.forEach((users,i) => {
    this.unseenFirebaseMessage = 0;
      this.updatedFirebaseData.forEach((messages,j) => {
         if(users.userId == messages.userId){
            if(messages.receiverId == this.userid){
              if(messages.userId != this.clickUser_UserId){
                this.unseenFirebaseMessage++;       
               }      
              //this.notification.push(this.unseenFirebaseMessage);
              this.newNotificationData[i] = this.unseenFirebaseMessage;
            }
         }
      })
  })

  this.otherUsers.forEach((users,i) => {
    this.finalUnseenNotificationDup[i] = this.newNotificationData[i] - this.notification[i];
        if( this.finalUnseenNotificationDup[i] > 0){
          this.finalUnseenNotification[i] =  this.finalUnseenNotificationDup[i];
          this.ref.detectChanges();
          // console.log( this.finalUnseenNotification[i])
        }
  })
 
}
changeprofile(event){
    this.selectedFile = event.target.files;
    let file = this.selectedFile.item(0);
    this.currentUpload = new Upload(file);
    this.chatservice.uploadProfile(this.currentUpload);
}
go(){
    let logUserFireKey,i,j,k;
    let loggeduserid = this.userid;
    let keys = Object.keys(userData);
    this.firebaseRegisterUserskey = keys;
    for(i=0;i<this.users.length;i++){
     // console.log("1111")
        if(this.users[i].userId == loggeduserid){   
          logUserFireKey = keys[i];
          this.leggedUserData = {
            userId : loggeduserid,
            name : this.users[i].name,
            email : this.users[i].email,
            password : this.users[i].password
          }
          break;
        }
    }
    this.chatservice.updateUrlRegister(logUserFireKey,this.leggedUserData).subscribe();
}
downloadFile(data){
  // this.downloadImage = true;
  // this.ref.detectChanges();

  // let link = document.createElement('a');
  // link.textContent = 'download image';
  
  // link.addEventListener('click', (ev) => {
  //     link.href = data.toDataURL();
  //     link.download = "mypainting.png";
  // }, false);

  // document.body.appendChild(link);
}
async onkey(event : any){
  this.ngName = (event.target.value).toLowerCase();
 
  // const rootRef = firebase.database().ref('Registered');
  // const oneRef = rootRef.orderByChild('name').startAt(this.ngName).endAt(this.ngName+'\uf8ff').on('value',(data) => {
  //     console.log(data.val());
  //    })
  this.showRecord = false;
  if(this.ngName != ""){
    this.searchUserDataObject = await this.getSpecificUser(this.ngName);
    // this.chatservice.searchUser().subscribe()
     this.searchUserDataArray = Object.keys(this.searchUserDataObject).map((key) => { return this.searchUserDataObject[key]});
    
     this.searchUserDataArray.forEach((data,i) => {
       if(data.userId == this.userid){    
          this.searchUserDataArray.pop();
       }
     })
  }else{

    this.showRecord = true;
  }
// this.ref.detectChanges();
 }
 getSpecificUser(data){
     return new Promise((resolve,reject) => {
       this.chatservice.searchUser(data).subscribe((data) => resolve(data))           
      }) 
 }
 
  async getUser(arg){
    let k,i,j,userDatalen;
   // console.log("arg in get user")
   // console.log(arg);
   userDatalen = this.userDataObject.length;
    
// --------------------  GET LOGGED USER DATA FROM REGISTER TABLE --------------//
    for(i=0;i<this.users.length;i++){
        //----------- Match loginuser userid and all user userid ----------------//
          if(this.users[i].userId == this.userid){
            this.loggedUser = this.users[i].name; 
            this.loggedUserProfile = this.users[i].url;
          }
    }   
    
    if(arg != undefined){
      this.contactTableDataArray = Object.keys(arg).map((key) => { return arg[key]});    
    }
 
// ------------------ GET DATA FROM CONTACT TABLE -------------------- //
// --------------- TO GET ONLY CONTACTABLE USERS ---------------- //
    this.contactTableDataArray.forEach((data,i) => {
 // --------- to check if i send any message ? ----------- //     
       if(data.userId == this.userid){        
// ------------ whose user that i send message to those ----------------- //         
          this.receiverIDArray.push(data.receiverId)
        //  console.log("receiver id @@@")
        //  console.log(this.receiverIDArray);     
          this.getContactUser(data.receiverId);   
      
       }
 //------------- if any user send me a message ? ------------ //    
         if(data.receiverId == this.userid){     
       // this.receiverIDArray.forEach((data,j) => {
 //------------  if any user send me a message then check thouse user is who that i send message ? --------- //
 //----------- if not then push those user userid ------------------//       
         // if(data.userId != data){
                // -------------- users who send message to me --------------------//        
         this.userIDArray.push(data.userId)
         this.getContactUser(data.userId)
       }
   });
   //this.findLastMessage();
// console.log("my connecte uers ")
// console.log(this.otherUsers) 

}

 async getContactUser(argID){
   let flagUser = 0;
    let i,j;
      for(i=0;i<this.users.length;i++){
        if(this.users[i].userId == argID){
            if(this.otherUsers.length == 0){
              this.otherUsers.push(this.users[i])
              this.ref.detectChanges();       
              break;
            }else{
              for(j=0;j<this.otherUsers.length;j++){             
                  if(this.otherUsers[j].userId != argID){
                  flagUser = 1;               
                  }else{                  
                    flagUser = 0;                 
                    break;
                  }
              }
              if(flagUser == 1){
              this.otherUsers.push(this.users[i]);
              this.ref.detectChanges();
              }
            }
        }
      }    
  }

  /* my changes data */
  getuserData(){
    return new Promise((resolve,reject) => {
      this.chatservice.getData().subscribe(
        (res: any) => 
        resolve(res)
      );
    })
  }
  async getReceiverNew(profile,id){
    let flag = 0,j,i,k;
   // (<HTMLInputElement>document.getElementById("message")).value = "";
   document.getElementById("message").remove();
   this.finalUnseenNotification.splice(id, 1);
 //  this.finalUnseenNotification[id] = 0;
    this.clickuserName = profile.name;
    this.clickUser_UserId = profile.userId;
    this.changeProfile = 1;
  
    this.getClickUserData = profile; 

    this.getUrlClickUser = profile.url;
    if(this.showRecord == false){
        this.insertSearchUserData = {
          userId : this.userid,
          receiverId :  profile.userId,
          Accept : 'false'
        }
        this.insertSearchUserDataTrued = {
          userId : this.userid,
          receiverId :  profile.userId,
          Accept : 'true'
        }
        for(j=0;j<this.contactTableDataArray.length;j++){
      // ---------- to check for clicke user is not receiver for any user --------------//   
      // --------- check clicked name userid and receiver id of response from contact table  --------- //
              if(profile.userId != this.contactTableDataArray[j].receiverId){
                // ----- till no record match flag = 1 ------ //
                flag = 1;
              }else{
    // ------ if clicked user is reciver for any user then check that case sender is logged-in user or not------ //                 
                  if(this.userid == this.contactTableDataArray[j].userId){
                    flag = 0;
                    break;
                  }else{
                    flag = 1;
                  }
              }       
         }
        if(flag == 1){
          await this.chatservice.contact(this.insertSearchUserData).subscribe(data => resolve(data))
           
        }

    }
   
    this.getReceiver(profile,id);
    
  }

  async getReceiver(dataArg,id){

  //  this.findLastMessage()
   // console.log("my new users")
   // console.log(this.userDataObject)
// ------------ change the userDataObject instead of otherUsers -------------- //
    let i,j,k,l,flag = 0,p=0;
    this.checkname = id;
    //present
    this.ref.detectChanges(); 
  
    this.getTempData = dataArg;
    if(dataArg != undefined){
      console.log(dataArg.userId,this.clickUser_UserId)  
      //if(dataArg.userId == this.clickUser_UserId ) {
     // this.checkname = id;
    // ------------ other users  -------------- //
     // for(i=0;i<this.otherUsers.length;i++){
      for(i=0;i<this.userDataObject.length;i++){
     // -------------- check other user index and click on name(index) -------------- //
     // if(i == id){
        if(this.userDataObject[i].userId == dataArg.userId){    
         // ----------- when login user click on name then find userid of other user and asign it's userid to recvId vareiable --------------//
        // ----------- clicked userid assign  ----------- //
          this.recvId = this.userDataObject[i].userId;
          this.contactData = {
              userId : this.userid,
              receiverId :  this.userDataObject[i].userId,
              Accept : 'false'
          }   
   //--------- get response of contact table and asign it to contactResponseData ----------- //
           let contactResponseData =  <any>await this.callcontactMethod().then((res) =>resolve(res));
      
           let keys = Object.keys(contactResponseData);
           this.firebasekey = keys;
      
  //--------   find key from firebase  ------------- //        
          /* for(k=0;k<keys.length;k++){
             console.log(keys[k]);
             //console.log(this.contactResponseModelData[k].Accept)
           }*/
            let array = Object.keys(contactResponseData).map((key) => { return contactResponseData[key]});
            this.contactDataArray = array;
            // ------- call messageResponse method for getting message --------- //
            let getResponse  = <any> await this.getMessage()
            let messageResponsearray = Object.keys(getResponse).map((key) => { return getResponse[key]});
            this.messageDataArray = messageResponsearray;
 //-------- delete all message before display message --------//
            // -------- this is only for not saw message every time when login user click other user ---------//
            this.deleteDisplayMessageLength = this.displayMessage.length;
            for(k=0;k< this.deleteDisplayMessageLength;k++){
                this.displayMessage.pop();
             }
            //------- initially usermessage is empty so we make condition ----------- // 
             if(this.userNewMessage.length != 0){
               // ----------- asign usernewmessage length to deleteusernewMessageLength -------//
               // ------ because in for loop we delete record so it effect on for loop ---------//
                this.deleteUerNewMessageLength = this.userNewMessage.length;
                for(k=0;k< this.deleteUerNewMessageLength;k++){
                   this.userNewMessage.pop();
                }  
             }
             this.changeProfile = 0;         
             for(j=0;j<messageResponsearray.length;j++){
               // -------- check clicked user userid and get message response userid  ---------//
               // ----- only check for clicked user send any messages ---- //
                if(this.userDataObject[i].userId == messageResponsearray[j].userId){
                  // ----- clicked user send message to login user  ------//
                    if(messageResponsearray[j].receiverId == this.userid){        
                     for(k=0;k<array.length;k++){
                       this.contactUserId = messageResponsearray[j].userId;
                       this.contactReceiverId = messageResponsearray[j].receiverId;                  
      // ------- check contact table senderid and message table sender id ------------ //
                        if(array[k].userId == messageResponsearray[j].userId){
      // ------ check conatact table receiver id and message table redceiver id ------------- //                    
                            if(array[k].receiverId == messageResponsearray[j].receiverId){
                                if(array[k].Accept == "false"){
                                  this.request = false;   
                                  this.ref.detectChanges();                              
                                }else{
                                  //console.log(messageResponsearray[j].userId);
                                  //console.log(messageResponsearray[j].receiverId);
                                  //console.log(dataArg.userId,this.userid)
      //-------- check message display only when logged user is sender and clicked user is receiver or logged user is receiver and clicked user is sender                
                                if((this.userid == messageResponsearray[j].userId && dataArg.userId == messageResponsearray[j].receiverId) || (this.userid == messageResponsearray[j].receiverId && dataArg.userId == messageResponsearray[j].userId)){
                               // if(dataArg.userId == messageResponsearray[j].userId || dataArg.userId == messageResponsearray[j].receiverId){
      //------------- only for if user send image then bydefault message is "" so,not display those type of image.therefor don't push those type of record-------------//
              // ----- in if condition message display and in else condition image display ------- //   
                                   
                                      this.changeProfile = 1;
                                      //console.log(messageResponsearray[j].time)
                                      this.clickedUserProfile = dataArg.url;   
                                     // messageResponsearray[j].time = this.convertTime( messageResponsearray[j].time)
                                      this.displayMessage.push(messageResponsearray[j]);
                                      //console.log( this.displayMessage);
                                      this.ref.detectChanges();                            
                                      disparray =  this.displayMessage;                                     
                                      this.displayMessageObser =   this.displayMessage;                                                              
                                      this.displayMessageAccept = this.displayMessage;                                   
                                      break; 
                                 //  }
                                  }         
                                }
                            }
                        }
                       }                 
                    }else{
                      // ------- if logeed user send any message to clicked user -------- //
                      /* -------- only for display send request button if clicked user no send message to logged in user
                                  and logged in user no send message to clicked user ---------    */
                        this.sendRequest = false;
                    }                   
                }
                    //------- login user send any message or not ------//
                    if(this.userid == messageResponsearray[j].userId){   
                      //-------  check login user send message to click user ---------//            
                        if(this.userDataObject[i].userId == messageResponsearray[j].receiverId){                      
                          //-------- check message display only when logged user is sender and clicked user is receiver or logged user is receiver and clicked user is sender                
                          // console.log(messageResponsearray[j].userId);
                          // console.log(messageResponsearray[j].receiverId);
                          // console.log(dataArg.userId,this.userid)
                          // console.log(messageResponsearray[j].message)
                        //  if((this.userid == messageResponsearray[j].userId && dataArg.userId == messageResponsearray[j].receiverId) || (this.userid == messageResponsearray[j].receiverId && dataArg.userId == messageResponsearray[j].userId)){                   
                           this.userNewMessage.push(messageResponsearray[j]);      
                            this.ref.detectChanges();      
                        //  }  
                        }
                    }
                
             }         
          //  console.log("display message is "+this.displayMessage)
            // ------- get response of contact table and access using for loop ------- //
              for(j=0;j<array.length;j++){
                //console.log("recorde is "+array[j].userId+ "and "+array[j].receiverId);
             // ---------- to check for clicke user is not receiver for any user --------------//   
            // --------- check clicked name userid and receiver id of response from contact table  --------- //
                    if(this.userDataObject[i].userId != array[j].receiverId){
                      // ----- till no record match flag = 1 ------ //
                      flag = 1;
                    }else{
    // ------ if clicked user is reciver for any user then check that case sender is logged-in user or not------ //                 
                        if(this.userid == array[j].userId){
                          flag = 0;
                          break;
                        }else{
                          flag = 1;
                        }
                     }       
             }
              if(flag == 1){
                this.chatservice.contact(this.contactData).subscribe(data => resolve(data))
                   
              }
        }    
    }
  //}
  
  }
 }
 

async acceptRequest(){
   let k,j,l,arg = 0;
  // console.log("Request is Accepted")
   this.request = true;
  //  let keys = Object.keys(this.contactTableData);
  //  this.firebasekey = keys;
   
  for(k=0;k<this.contactDataArray.length;k++){
      if(this.contactUserId == this.contactDataArray[k].userId){
          if(this.contactReceiverId == this.contactDataArray[k].receiverId){
              this.approveContactData = {
                     userId : this.contactUserId,
                     receiverId: this.contactReceiverId,
                     Accept:"true"
              }
              //console.log("accept 111")
            //  if(this.firebasekey[k] != undefined){
               await this.chatservice.updateContact(this.firebasekey[k],this.approveContactData).subscribe();//.subscribe(data => console.log(data));
            //  }
              this.findAcceptRequest(arg);
              this.request == true;
              for(j=0;j<this.messageDataArray.length;j++){
               
                // -------- check clicked user userid and get message response userid  ---------//
                // ----- only check for clicked user send any messages ---- //
                 if(this.recvId == this.messageDataArray[j].userId){
                 
                   // ----- clicked user send message to login user  ------//
                     if(this.messageDataArray[j].receiverId == this.userid){        
                   
                      for(k=0;k<this.contactDataArray.length;k++){
                      //  this.contactUserId = this.messageDataArray[j].userId;
                      //  this.contactReceiverId = this.messageDataArray[j].receiverId;
                       // console.log("Hello")
       // ------- check contact table senderid and message table sender id ------------ //
                         if(this.contactDataArray[k].userId == this.messageDataArray[j].userId){
       // ------ check conatact table receiver id and message table redceiver id ------------- //                    
                             if(this.contactDataArray[k].receiverId == this.messageDataArray[j].receiverId){
                                   this.displayMessage.push(this.messageDataArray[j]);
                                   this.ref.detectChanges();   
                                   this.displayMessageAccept = this.displayMessage;                                  
                                   break;                           
                             }
                         }
                        }                 
                     }else{
                       // ------- if logeed user send any message to clicked user -------- //
                       /* -------- only for display send request button if clicked user no send message to logged in user
                                   and logged in user no send message to clicked user ---------    */
                        this.sendRequest = false;
                     }                   
                 }
                 //------- login user send any message or not ------//
                 if(this.userid == this.messageDataArray[j].userId){   
                   //-------  check login user send message to click user ---------//            
                     if(this.recvId ==  this.messageDataArray[j].receiverId){                      
                         this.userNewMessage.push( this.messageDataArray[j].message);    
                         this.ref.detectChanges();                                         
                     }
                 }
              }
              break;
          }
      }
   }
   this.request = true;
   this.ref.detectChanges();

  //  let elemAccept = document.getElementById('accept_id');
  //  elemAccept.parentNode.removeChild(elemAccept);
  //  let elemRefuse = document.getElementById('refuse_id');
  //  elemRefuse.parentNode.removeChild(elemRefuse);
   
  }
 async findAcceptRequest(arg){
   if(arg != 0){
  this.contactTableDataArray = Object.keys(arg).map((key) => { return  arg[key]});
   }
  let Newkeys = Object.keys(arg);
  this.Newfirebasekey = Newkeys;
    let j,fbkey;
    this.approveReq = false;
    this.insertSearchUserDataTrued = {
      userId : this.userid,
      receiverId : this.clickUser_UserId,
      Accept : 'true'
    }
    for(j=0;j<this.contactTableDataArray.length;j++){
      // check logged-in user is receiver of any user ?
      if(this.userid == this.contactTableDataArray[j].receiverId){
        // if loggedin user is receiver of any user then check sender is clicked user ?
        if(this.contactTableDataArray[j].userId ==  this.clickUser_UserId){
        // if clicked user send me a request then pass true in request.
         // this.approveReq = true;
          if(this.contactTableDataArray[j].Accept == "true"){
            this.approveReq = true;       
          }
        }
      }
    }
  //  console.log("find request method ")
  //  console.log(this.contactTableDataArray)
  //  console.log(this.clickUser_UserId)
 if(this.approveReq == true){
    for(j=0;j<this.contactTableDataArray.length;j++){
    
      if(this.userid == this.contactTableDataArray[j].userId){
        //console.log("1 if")
        if(this.contactTableDataArray[j].receiverId ==  this.clickUser_UserId){
         // console.log("2 if ")
         // console.log("My update key is ")
         // console.log(this.Newfirebasekey[j])
          if(this.Newfirebasekey[j] != undefined){
           // console.log(this.Newfirebasekey[j])
         this.chatservice.updateContactNew(this.Newfirebasekey[j], this.insertSearchUserDataTrued).subscribe();
         break;
          }
        }
      }
    }  
  }
 
} 
  refuseRequest(){
    this.request = true;
    this.ref.detectChanges();
   // console.log("Request is Refused")
    // let elemAccept = document.getElementById('accept_id');
    // elemAccept.parentNode.removeChild(elemAccept);
    // let elemRefuse = document.getElementById('refuse_id');
    // elemRefuse.parentNode.removeChild(elemRefuse);
    return false;
  }

  handleFileInput(event) {
  // ---------- this is handlefileInput method --------- //  
  //  console.log("file upload "); 
    this.selectedFile = event.target.files;
 /* -------- this is go method -------*/
    let file = this.selectedFile.item(0);
    this.currentUpload = new Upload(file);
    this.upload = {
      userId :  this.userid,
      receiverId :this.recvId,
      message : "",
      type : "",
      date : this.fullDate,
      time : this.time  
    }
    this.chatservice.UploadFile(this.currentUpload,this.upload); 
   }

  callcontactMethod(){
   // console.log("call contact method")
    return new Promise((resolve,reject) => {
      this.chatservice.checkReceiverId().subscribe((data) =>resolve(data));
    })
  }
  getMessage(){
    return new Promise((resolve,reject) => {
      this.chatservice.getMessages().subscribe(data => resolve(data));
    })
  }
  logoutUser(){
    //console.log("user logout");
    localStorage.removeItem('idToken');
    //localStorage.clear();
    this.navroute.navigate(['/Login']);
  }

  checkUserLogged(){
    if(localStorage.idToken == null){
       this.navroute.navigate(['/Login']);
    }
    else{
      this.userid = this.route.snapshot.paramMap.get('userid');
      console.log(this.userid)
    }
  }
  convertTime(time){
    console.log("time method")
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join ('');
  }
  async submitMessage(msg){
   // console.log("submit data ")
    if(this.recvId == undefined){
      this.recvId = this.getClickUserData.userId;
    }
  
    if(msg.value != ""){
      this.d = new Date();
      this.date = this.d.getDate();
      this.month = this.d.getMonth()+1;
      this.year = this.d.getFullYear();
      this.hour = this.d.getHours();
      this.minute = this.d.getMinutes();
      this.seconds =  this.d.getSeconds();
      this.time = this.hour+":"+this.minute+":"+this.seconds;   
   //   this.time =  await this.convertTime(this.time); 
      this.fullDate = this.year+"-"+this.month+"-"+this.date+" "+this.hour+":"+this.minute+":"+this.seconds+".0";

    //  console.log(this.fullDate)
    
      this.messageData = {
          userId : this.userid,
          receiverId : this.recvId,
          message : msg.value,
          type : 'IND',
          name : "",
          url : "",
          date : this.fullDate,
          time : this.time     
      }
      this.userNewMessage.push(msg.value);
    //  this.ref.detectChanges();   
      await this.chatservice.putMessages(this.messageData).subscribe();
     // this.getReceiver(this.getTempData,this.checkname);  
      (<HTMLInputElement>document.getElementById("txt")).value = "";
    }
  }

}
