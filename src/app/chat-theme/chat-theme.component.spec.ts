import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatThemeComponent } from './chat-theme.component';

describe('ChatThemeComponent', () => {
  let component: ChatThemeComponent;
  let fixture: ComponentFixture<ChatThemeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatThemeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatThemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
