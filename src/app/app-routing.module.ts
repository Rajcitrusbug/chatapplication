import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserRegisterComponent} from './user-register/user-register.component';
import {UserLoginComponent} from './user-login/user-login.component';
import {LayoutComponent} from './layout/layout.component';
import {ChatThemeComponent} from './chat-theme/chat-theme.component';
import {UserprofileComponent} from './userprofile/userprofile.component';
import {NewchatComponent} from './newchat/newchat.component';
const routes: Routes = [
 {path : '',component : LayoutComponent},
 
 // children:[
    {path : '',redirectTo : '/Register' , pathMatch : 'full'},
    {path : 'Register',component : UserRegisterComponent},
    {path : 'Login',component : UserLoginComponent},
    {path : 'display/:userid' , component : ChatThemeComponent},
    {path : 'newchat/:userid',component : NewchatComponent},
    {path : '',component : UserLoginComponent},
    {path : 'openpage' , component : ChatThemeComponent},
    {path : 'profile/:userid', component : UserprofileComponent},
    {path: '**', redirectTo: '/Login'}
 // ]
// }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
