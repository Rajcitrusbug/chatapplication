import { Component, OnInit } from '@angular/core';
import {ChatserviceService} from '../chatservice.service';
import { Routes, RouterModule } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import { AngularFireAuth } from  "@angular/fire/auth";
import { User } from  'firebase';
import { resolve } from 'q';
//import { Router } from '@angular/router';  
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { User } from './user';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  email : string;
  password : string;
  users : any;
  flag : boolean = false;
  object: any;
  getArrayResponse : any;
  //user: User;
  getPerArray : any;
  getidArray : any;
  userObject : any;
  unamePattern : any = "^[a-z0-9_-]{8,15}$"; 
  user : any;
  constructor(private chatservice : ChatserviceService,  private route: Router,public  afAuth:  AngularFireAuth) { }

  ngOnInit() {
    console.log("ng on init of login")
    if(localStorage.idToken != null){
      localStorage.removeItem('idToken');
      console.log("remove item");
    }
  }
  async SubmitData(arg1,arg2){
    this.user = arg1.value;
    this.email = arg1.value;
    this.password = arg2.value;
    //let data
  
          this.object = {
            email : arg1.value,
            password : arg2.value,
            "returnSecureToken":true
          }
    
          let getResponse = <any>await this.getSigninMethod().then(res=> resolve(res));
          let userid = getResponse.localId;
        
          this.userObject = {
            "idToken" : getResponse.idToken
          }
         localStorage.setItem('idToken', getResponse.idToken);
         let getUser = <any>await this.getLoggedUserData(this.userObject).then(res =>resolve(res));      
         console.log(getUser)
         // this.route.navigate(['/display',userid]);
         this.route.navigate(['/newchat',userid]);
     
   }
    getSigninMethod(){
     return new Promise((resolve,reject) => {
     this.chatservice.signin(this.object).subscribe(data => resolve(data))
     })    
    }
  
  getLoggedUserData(arg1){
      return new Promise((resolve,reject) => {
        this.chatservice.getLoggedUserData(arg1).subscribe(data => resolve(data))
    })
   }
  
}

/*
email address must contain at least a ‘@’ sign and a dot (.). 
Also, the ‘@’ must not be the first character of the email address, 
and the last dot must at least be one character after the ‘@’ sign.

*/