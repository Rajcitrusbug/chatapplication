import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import {ChatserviceService} from '../chatservice.service';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})

export class LayoutComponent implements OnInit {

 flag : boolean = false;
  constructor(private chatservice : ChatserviceService,  private route: ActivatedRoute) { }

  ngOnInit() {

  }
  ngDoCheck(){
    if(localStorage.idToken != null){
      this.flag = true;
    }else{
      this.flag = false;
    }
  }
  logoutUser(){
    console.log("user logout");
    localStorage.removeItem('idToken');
    //localStorage.clear();
   // this.navroute.navigate(['/Login']);
  }
  
}
