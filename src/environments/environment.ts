// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import * as firebase from 'firebase/app';


export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCNXFJKypuSfxyz8j8ppU7sVlvaZPQ8Lrs",
    authDomain: "fir-demo1-16145.firebaseapp.com",
    databaseURL: "https://fir-demo1-16145.firebaseio.com",
    projectId: "fir-demo1-16145",
    storageBucket: "fir-demo1-16145.appspot.com",
    messagingSenderId: "845999137950",
  },
  
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
//gs://fir-demo1-16145.appspot.com/